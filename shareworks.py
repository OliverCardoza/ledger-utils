# Script to convert Shareworks stock data into ledger format.

import argparse
import csv
import dataclasses
import datetime
import decimal
import price
import re

@dataclasses.dataclass
class ReleaseEvent:
  award_date: datetime.datetime = None
  grant_date: datetime.datetime = None
  plan_name: str = None
  grant_num: str = None
  award_name: str = None
  release_date: datetime.datetime = None
  release_price_usd: decimal.Decimal = None
  units_vested: int = None
  units_released: int = None
  units_withheld: int = None
  release_method: str = None
  taxable_compensation_usd: decimal.Decimal = None
  taxes_usd: decimal.Decimal = None
  released_value_usd: decimal.Decimal = None
  federal_taxes_usd: decimal.Decimal = None
  provincial_taxes_usd: decimal.Decimal = None
  cpp_usd: decimal.Decimal = None
  ei_usd: decimal.Decimal = None
  qpp_usd: decimal.Decimal = None
  international_tax_usd: decimal.Decimal = None


class ShareworksCsvParser(object):
  def __init__(self, release_detail_file):
    self.release_detail_file = release_detail_file
    self.release_events = []

  def Parse(self):
    with open (self.release_detail_file, newline='') as f:
      reader= csv.reader(f)
      for idx, row in enumerate(reader):
        if idx <= 2:
          continue
        if len(row) == 1:
          # ignore final row
          continue
        self.release_events.append(self._ParseReleaseEvent(row))

  def _ParseReleaseEvent(self, row):
    release_event = ReleaseEvent(
        award_date = self._ParseDate(row[0]),
        grant_date = self._ParseDate(row[1]),
        plan_name = row[2],
        grant_num = row[3],
        award_name = row[4],
        release_date = self._ParseDate(row[5]),
        release_price_usd = self._ParseDecimal(row[6]),
        units_vested = int(row[7]),
        units_released = int(row[8]),
        units_withheld = int(row[9]),
        release_method = row[10],
        taxable_compensation_usd = self._ParseDecimal(row[11]),
        taxes_usd = self._ParseDecimal(row[12]),
        released_value_usd = self._ParseDecimal(row[13]),
        federal_taxes_usd = self._ParseDecimal(row[14]),
        provincial_taxes_usd = self._ParseDecimal(row[15]),
        cpp_usd = self._ParseDecimal(row[16]),
        ei_usd = self._ParseDecimal(row[17]),
        qpp_usd = self._ParseDecimal(row[18]),
        international_tax_usd = self._ParseDecimal(row[19]))
    return release_event

  def _ParseDate(self, date_str):
    return datetime.datetime.strptime(date_str, "%d-%b-%Y")

  def _ParseDecimal(self, val):
    val = re.sub('[$,"]', '', val)
    return round(decimal.Decimal(val), 2)

  def Print(self):
    template_str = """{date} * Stock Release
    ; Grant ID: {grant_id}
    ; 1 VAU = {vau_unit_usd} USD
    ; 1 USD = {usd_unit_cad} CAD
    ; 1 VAU = {vau_unit_cad} CAD
    Income:Pre-tax:Equity                        CAD -{taxable_income}
    Assets:Brokerage:Shareworks                  VAU {units_released} @ CAD {vau_unit_cad}
    Income:Deductions:Income Tax Cover           CAD {taxes}
    Income:Pre-tax:VAU Offset                    CAD {offset}
    Equity:Rounting Errors                       CAD {rounding_err}
    (Virtual:ACB:VAU)                            CAD {acb}
"""

    self.release_events.sort(key=lambda event: event.release_date.strftime("%Y-%m-%d") + event.grant_num)
    for event in self.release_events:
      usd_unit_cad = price.GetPrice('USD', event.release_date, search_forward=False).amount_in_cad
      vau_unit_cad = round(usd_unit_cad * event.release_price_usd, 4)
      taxable_income = round(event.taxable_compensation_usd * usd_unit_cad, 4)
      taxes = round(event.taxes_usd * usd_unit_cad, 4)
      offset = event.units_withheld * vau_unit_cad - taxes
      acb = event.units_released * vau_unit_cad 
      rounding_err = taxable_income - taxes - offset - acb
      if abs(rounding_err) >= 1:
        raise Exception("rounding error to big: {0}".format(rounding_err))
      print(template_str.format(**{
        "date": event.release_date.strftime("%Y-%m-%d"),
        "grant_id": event.grant_num,
        "vau_unit_usd": event.release_price_usd,
        "usd_unit_cad": usd_unit_cad,
        "vau_unit_cad": vau_unit_cad,
        "taxable_income": taxable_income,
        "units_released": event.units_released,
        "taxes": taxes,
        "offset": offset,
        "rounding_err": rounding_err,
        "acb": acb,
      }))


parser = argparse.ArgumentParser(description='Converts Shareworks csv files into ledger-cli format')
parser.add_argument('release_detail_file', type=str, 
                    help='Address of the release detail csv file which will be parsed.')


if __name__ == '__main__':
  args = parser.parse_args()
  shareworks_parser = ShareworksCsvParser(args.release_detail_file)
  shareworks_parser.Parse()
  shareworks_parser.Print()

