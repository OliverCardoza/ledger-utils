# Script to get prices for currency conversion or stock unit values.
#
# Supported quote types:
# - USD/CAD conversion from bankofcanada.ca
# - Select Horizon and Vanguard ETF prices from ca.finance.yahoo.com
#
# Example usage of bulk download:
#     python3 price.py VAB -s 2015-01-01 -e 2015-12-31
#
# Example usage within Python code:
#     import price
#     import datetime
#     end = datetime.datetime.today()
#     start = end - datetime.timedelta(days = 5)
#     prices = price.GetPrices('USD', start, end)
#     for price in prices:
#       print(price.ToLedgerFormat())

import argparse
import datetime
import decimal
import urllib.request
import yfinance

BANK_OF_CANADA_URL='http://www.bankofcanada.ca/valet/observations/FXUSDCAD/csv?start_date={start_date}&end_date={end_date}'
YAHOO_SYMBOL_TO_URL_PARAM = {
  'DLR_TO': 'DLR.TO',
  'XBT': 'BTC-CAD',
  'ETH': 'ETH-CAD',
  'VAB': 'VAB.TO',
  'VCN': 'VCN.TO',
  'VXC': 'VXC.TO',
}
PRECISION = 4


class Price(object):
  def __init__(self, stock_symbol, date, amount_in_cad):
    self.stock_symbol = stock_symbol
    self.date = date
    self.amount_in_cad = decimal.Decimal(amount_in_cad)

  def ToLedgerFormat(self):
    return 'P {0} {1} CAD {2}'.format(
        self.date.strftime('%Y-%m-%d'), self.stock_symbol, self.amount_in_cad)


def GetPrice(stock_symbol, date, search_forward=None):
  '''Returns a Price for the specified date and commodity.

  Args:
      stock_symbol: The stock or currency code to lookup price data on.
      date: The date to lookup conversion date for.
      search_forward: If specified then when no data is available for the given day we will
          find the next conversion moving one day in this direction.

  Returns:
      A Price instance or None if no data was found.
  '''
  attempts = 0
  while attempts < 10:
    attempts += 1
    prices = GetPrices(stock_symbol, date, date)
    if len(prices) == 1:
      return prices[0]
    elif len(prices) > 1:
      raise ValueError('Should not have more than one price for a single day')
    elif len(prices) == 0 and search_forward is None:
      return None
    elif len(prices) == 0 and search_forward:
      date = date + datetime.timedelta(days=1)
    elif len(prices) == 0 and not search_forward:
      date = date - datetime.timedelta(days=1)
    else:
      raise ValueError('Invalid prices size')
  raise ValueError('Made 10 attempts to lookup price data and all failed.')


def _GetNearestWeekDay(date, search_forward=True):
  # Weekday: 0=Monday, 6=Saturday
  if date.weekday() < 5:
    return date
  elif search_forward:
    # shift to monday 
    forward_delta = datetime.timedelta(days=(7 - date.weekday()))
    return date + forward_delta
  else:
    # shift to friday
    backward_delta = datetime.timedelta(days=(date.weekday() - 4))
    return date - backward_delta


def _GetUsdBankOfCanadaRates(start_date, end_date):
  '''Returns a list of Price objects containing USD conversion data.

  This script uses the noon price posted by the Bank of Canada. Starting March 1, 2017 the Bank of
  Canada will switch to only publishing a single daily exchange rate using a method to be
  determined.
  '''
  # Shift each date to the nearest weekday since the BoC does not publish conversions for
  # weekends.
  weekday_start = _GetNearestWeekDay(start_date, search_forward=True)
  weekday_end = _GetNearestWeekDay(end_date, search_forward=False)
  if weekday_start > weekday_end:
    return []

  start_string = weekday_start.strftime('%Y-%m-%d')
  end_string = weekday_end.strftime('%Y-%m-%d')
  url = BANK_OF_CANADA_URL.format(start_date=start_string, end_date=end_string)
  response = urllib.request.urlopen(url)
  # Convert response to string, remove raw/binary prefixes, split on newline, remove junk rows.
  csv_lines = str(response.read()).strip("b'").split('\\n')[9:]
  prices = []
  for line in csv_lines:
    # Strip whitespace and quotes which seem to have been added to the data starting 2019-08.
    values = [value.strip('" \\r') for value in line.split(',')]
    # Exchange rates come first, followed by errors. Once "ERRORS" is seen
    # we can return early.
    if values[0] == 'ERRORS':
      return prices
    # Bank holidays and lack of data are expressed as the date in column 1 with empty in column
    # 2.
    if len(values) < 2 or not values[1]:
      continue

    date = datetime.datetime.strptime(values[0], '%Y-%m-%d')
    price = values[1]
    prices.append(Price('USD', date, price))
  return prices 


def _GetYahooPrices(stock_symbol, start_date, end_date):
  '''Returns a list of Price objects containing Yahoo price data for the given symbol.

  This script uses the close price for each day.
  '''
  yahoo_symbol = YAHOO_SYMBOL_TO_URL_PARAM[stock_symbol]
  ticker = yfinance.Ticker(yahoo_symbol)
  data = ticker.history(start=start_date.strftime('%Y-%m-%d'),
      end=end_date.strftime('%Y-%m-%d'))
  prices = []
  for item in data['Close'].items():
    date = item[0].to_pydatetime()
    price = round(decimal.Decimal(item[1]), PRECISION)
    prices.append(Price(stock_symbol, date, price))
  return prices


def GetPrices(stock_symbol, start_date, end_date):
  '''Returns a list of price data for the following commodity and date period.

  If both dates are the same then a single conversion rate is returned for that date.'''
  if stock_symbol == 'USD':
    return _GetUsdBankOfCanadaRates(start_date, end_date)
  else:
    return _GetYahooPrices(stock_symbol, start_date, end_date)


def _ValidDate(date_arg):
  try:
    return datetime.datetime.strptime(date_arg, '%Y-%m-%d')
  except ValueError:
    msg = 'Not a valid date: {0}'.format(date_arg)
    raise argparse.ArgumentTypeError(msg)


parser = argparse.ArgumentParser(description='Downloads USD conversion data')
parser.add_argument('stock_symbol', type=str,
                    choices=['USD', 'VAB', 'VCN', 'VXC', 'DLR_TO', 'XBT', 'ETH'],
                    help='The symbol of the stock or currency to retrieve a quote on.')
parser.add_argument('-s', '--start_date', type=_ValidDate, required=True,
                    help='Start date for downloading currency conversions in YYYY-mm-dd format.')
parser.add_argument('-e', '--end_date', type=_ValidDate, required=True,
                    help='End date (inclusive) for downloading currency conversions in ' +
                        'YYYY-mm-dd format.')


if __name__ == '__main__':
  args = parser.parse_args()
  start_date_str = args.start_date.strftime('%Y-%m-%d')
  end_date_str = args.end_date.strftime('%Y-%m-%d')
  print('; Symbol:       {0}'.format(args.stock_symbol))
  print('; Start date:   {0}'.format(start_date_str))
  print('; End date:     {0}'.format(end_date_str))
  prices = GetPrices(args.stock_symbol, args.start_date, args.end_date)

  print('; Values:       {0}'.format(len(prices)))
  for price in prices:
    print(price.ToLedgerFormat())

      
