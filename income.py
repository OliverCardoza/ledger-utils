# Python script to parse a Google Ultipro paystub.
# 
# Example usage:
# find ./2015/income_ultipro/2015-02-*.pdf | xargs -I % python3 income.py % > \
#      ./2015/income_ultipro/1502_income_ultipro.ledger

import argparse
import collections
import datetime
import decimal
import re
import subprocess
import utils

from pdf_parser import PdfParser


# A struct to encapsulate a single earning/deduction from an income paystub.
Posting = collections.namedtuple('Posting', ['account', 'amount'])

# The account prefix used by default for all entries in the earnings list.
EARNINGS_PREFIX = "Income:Pre-tax:"
# The account prefix used by default for all entries in the deductions list.
DEDUCTIONS_PREFIX = "Income:Deductions:"
# The account your pay is deposited into.
DEPOSIT_ACCOUNT = "Transfers:Chequing:TD"
# The account where RRSP contributions should go.
RRSP_ACCOUNT = "Transfers:RRSP:Manulife"
# The account where charitable contributions go.
CHARITY_ACCOUNT = "Expenses:Charity"
# Accounts matched by the earnings or deduction pattern but should be ignored.
ACCOUNT_BLACKLIST = ["Current", "YTD"]

PATTERN_VARS = {
  "account": "\w+[\w &]+\w+",
  "number": "-?[\d,.]+",
  "money": "\(?\$[\d,.]+\)?",
  "date": "\d+/\d+/\d+"
}
EARNINGS_PATTERN_SUFFIX = "\s+(?:{number}\s+{money}\s+)?({money})\s+{money}".format(
    **PATTERN_VARS)
EARNINGS_PATTERN_PRE_2018 = "\s({account})\s+{number}\s+{money}\s+({money})\s+{money}".format(
    **PATTERN_VARS)
EARNINGS_STOCK_PATTERN = ("\s({account})\s+{number}\s+{money}\s+{number}\s+{money}\s+"
                          "({money})\s+{money}").format(**PATTERN_VARS)
DEDUCTIONS_PATTERN_SUFFIX = "\s+(?:{money})?\s+({money})\s+{money}\s+{money}\s+{money}".format(**PATTERN_VARS)
DEDUCTIONS_PATTERN_PRE_2018 = "\s({account})\s+(\(?{money}\)?)\s+\(?{money}\)?".format(
    **PATTERN_VARS)
# The following patterns are used to parse the pay summary footer.
TOTAL_GROSS_PATTERN = "Current\s+({money})\s+{money}\s+{money}".format(**PATTERN_VARS)
TOTAL_DEDUCTIONS_PATTERN = "Current\s+{money}\s+({money})\s+{money}".format(**PATTERN_VARS)


class IncomeParser(object):
  def __init__(self, paystub_file, mark_stock_virtual, skip_validation):
    self.pdf_parser = PdfParser(paystub_file)
    self.paystub_file = paystub_file
    self.mark_stock_virtual = mark_stock_virtual
    self.skip_validation = skip_validation
    self.earnings_account_map = {}
    self.earning_postings = []
    self.deductions_account_map = {}
    self.deduction_postings = []

    self.period_start = self._GetDate("Period Start Date\s+")
    self.period_end = self._GetDate("Period End Date\s+")
    self.pay_date = self._GetDate("Pay Date\s+")
    self.document_id = self.pdf_parser.ParseString("Document\s+(\w+)")
    self.total_net_pay = self._GetNetPay()

  def _GetDate(self, match_prefix):
    """Returns a datetime extracted from the pdf with the given match prefix.

    The date format in Ultipro statements changed after 2017-07. It used to be represented as
    "MM/DD/YYYY" but is now formatted as "MM DD YYYY". This function will try both.

    Args:
        match_prefix: The regex string match before the desired date.

    Returns:
        A datetime parsed from the pdf.
    """
    default_date_fmt = "%Y %m %d"
    default_date_regex = "(\d+ \d+ \d+)"
    legacy_date_fmt = "%m/%d/%Y"
    legacy_date_regex = "(\d+/\d+/\d+)"
    try:
      return self.pdf_parser.ParseDate(
          match_prefix + default_date_regex, date_fmt=default_date_fmt)
    except ValueError:
      return self.pdf_parser.ParseDate(
          match_prefix + legacy_date_regex, date_fmt=legacy_date_fmt)

  def _GetNetPay(self):
    """Returns the net pay as a decimal.

    The Ultipro stock statement format was changed after 2017-07. It used to contain a footer
    similar to non-stock statements under the format: "Current   $###.##   $###.##   $###.##"
    with the last being the net pay. After the changes there is a "Net Pay    $###.##" header
    field. Seems like the "Net Pay" parsing method is more reliable and handles all formats so
    that is used as the default extraction point.

    Returns:
        A decimal of the net pay.
    """
    default_regex = "Net Pay\s+\(?\$([\d,.]+)\)?"
    legacy_regex = "Current\s+\(?\$[\d,.]+\)?\s+\(?\$[\d,.]+\)?\s+\(?\$([\d,.]+)\)?"
    try:
      return self.pdf_parser.ParseCommodity(default_regex)
    except ValueError:
      return self.pdf_parser.ParseCommodity(legacy_regex)

  def _GetTotalGross(self):
    """Returns a decimal of the total gross earnings."""
    return self.pdf_parser.ParseCommodity(TOTAL_GROSS_PATTERN)

  def _GetTotalDeductions(self):
    """Returns a decimal of the total deductions."""
    return self.pdf_parser.ParseCommodity(TOTAL_DEDUCTIONS_PATTERN)

  def IsStockStub(self):
    """Returns true if this statement is for a stock vesting event.

    Older stock statements used to have document ids prefixed with RSUA. They changed this in
    2017-06 to only prefix UA. It looks like the document id was a fixed length field. In order to
    include more digits of the number they shortened the string type prefix. All stock statement
    document numbers before 2017-06 are missing the last 2 digits.
    """
    return bool(utils.pdfgrep("Document\s+(RS)?UA\d+", self.paystub_file))

  def _IsNewSalaryFormat(self):
    """Returns true if this is a salary paystub (not equity) from after August 2017.

    Starting in August 2017 the formatting all changed. This split allows reprocessing old
    and new files.
    """
    pay_datetime = datetime.datetime.strptime(self.pay_date, '%Y-%m-%d')
    format_change_datetime = datetime.datetime(2017, 8, 1)
    return not self.IsStockStub() and pay_datetime >= format_change_datetime

  def AddEarningAccount(self, name, ledger_account=None):
    self._AddAccount(name, ledger_account=ledger_account, is_earning=True)

  def AddDeductionAccount(self, name, ledger_account=None):
    self._AddAccount(name, ledger_account=ledger_account, is_earning=False)

  def _AddAccount(self, name, ledger_account=None, is_earning=True):
    account_dict = self.earnings_account_map if is_earning else self.deductions_account_map
    if ledger_account:
      account_dict[name] = ledger_account
    elif is_earning:
      account_dict[name] = EARNINGS_PREFIX + name
    else:
      account_dict[name] = DEDUCTIONS_PREFIX + name
    if self.IsStockStub() and self.mark_stock_virtual:
      account_dict[name] = "[Virtual:{0}]".format(account_dict[name])

  def ParsePostings(self):
    self._ParseEarnings()
    self._ParseDeductions()

  def Print(self):
    if not self.skip_validation:
      self._ValidateBeforePrint()
    template_str = """{0.pay_date} * Google paystub
    ; Pay period: {0.period_start} to {0.period_end}
    ; Document: {0.document_id}"""
    posting_template = "    {0: <68} CAD {1}"
    print(template_str.format(self))
    print(posting_template.format(DEPOSIT_ACCOUNT, self.total_net_pay))
    for posting in self.earning_postings:
      print(posting_template.format(posting.account, posting.amount))
    for posting in self.deduction_postings:
      print(posting_template.format(posting.account, posting.amount))
    print("")

  def PrintDebug(self):
    template_str = """; DEBUG INFO
; Total Gross:       {0}
; Total Deductions:  {1}
; Total Net:         {2}
"""
    print(template_str.format(
        self._GetTotalGross(), self._GetTotalDeductions(), self.total_net_pay))

  def _ParseEarnings(self):
    if self._IsNewSalaryFormat():
      self._ParsePostings(is_earnings=True)
    else:
      earnings_pattern = EARNINGS_STOCK_PATTERN if self.IsStockStub() else EARNINGS_PATTERN_PRE_2018
      num_terms_right_of_account = 6 if self.IsStockStub() else 4
      self._ParsePostingsLegacy(earnings_pattern, num_terms_right_of_account, True)
    if self.IsStockStub() and len(self.earning_postings) == 0:
      # Stock stubs starting in 2017-12 began printing the earnings columns in a very hard-to-parse
      # manner. To combat this extra handling is added here to verify there is only one earning
      # representing the entire earning sum and use that as the stock earning value.
      gross_amount_regex = "\\${0:,}".format(self._GetTotalGross())
      matches = utils.pdfgrep(gross_amount_regex, self.paystub_file)
      num_matches = len(matches.splitlines())
      if num_matches == 3:
        # Three matches for this dollar amount: total gross, total deductions, and the stock sale
        # amount which validates the assumption for this case (total gross matches total stock
        # sale).
        self._AddPosting("Google Stock", self._GetTotalGross(), is_earning=True)

  def _ParseDeductions(self):
    if self._IsNewSalaryFormat():
      self._ParsePostings(is_earnings=False)
    else:
      num_terms_right_of_account = 6 if self.IsStockStub() else 4
      self._ParsePostingsLegacy(DEDUCTIONS_PATTERN_PRE_2018, 2, False)

  def _ParsePostingsLegacy(self, posting_regex, num_terms_right_of_account, is_earning):
    """Parses postings by running a regex to returning all earnings/deductions and then processing
    each line as an individual posting.

    Args:
        posting_regex: Regex which will return multiple posting matches.
        num_terms_right_of_account: This many terms will be trimmed off the right of the match to
            extract the account.
        is_earning: True if the postings being processed are earnings (deductions otherwise).
    """
    posting_matches = utils.pdfgrep(posting_regex, self.paystub_file)
    for line in posting_matches.splitlines():
      terms = line.split()
      raw_account = " ".join(terms[:-num_terms_right_of_account])
      raw_amount = terms[-2]
      str_amount = re.sub("[\$,]", "", raw_amount)
      if str_amount.startswith("(") and str_amount.endswith(")"):
        str_amount = "-" + str_amount[1:-1]
      amount = decimal.Decimal(str_amount)
      self._AddPosting(raw_account, amount, is_earning)

  def _ParsePostings(self, is_earnings):
    """Parses postings by attempting to extract each account in the account maps."""
    account_map = self.earnings_account_map if is_earnings else self.deductions_account_map
    regex_suffix = EARNINGS_PATTERN_SUFFIX if is_earnings else DEDUCTIONS_PATTERN_SUFFIX
    # Iterate over keys sorted to get deterministic posting order.
    for account in sorted(account_map.keys()):
      regex = account + regex_suffix
      amount = self.pdf_parser.MaybeParseCommodity(regex)
      self._AddPosting(account, amount, is_earnings)

  def _AddPosting(self, raw_account, amount, is_earning):
    """Validates params and optionally adds the data to the posting_list.

    Args:
        raw_account: The account name as it appears in the paystub.
        amount: None or a Decimal value representing the posting amount.
        is_earning: True if this posting is an earning else represents a deduction.
    """
    # A known account that should be ignored.
    if raw_account in ACCOUNT_BLACKLIST:
      return
    if not amount or amount == 0:
      return
    # For double entry accounting earning accounts should be negative amounts.
    if is_earning:
      amount = amount * -1

    if is_earning and raw_account not in self.earnings_account_map:
      raise ValueError(
          "Found an earning account not whitelisted: {0} in {1}".format(
              raw_account, self.paystub_file))
    if not is_earning and raw_account not in self.deductions_account_map:
      raise ValueError(
          "Found a deduction account not whitelisted: {0} in {1}".format(
              raw_account, self.paystub_file))
    account = self.earnings_account_map[raw_account] if is_earning else self.deductions_account_map[raw_account]
    posting_list = self.earning_postings if is_earning else self.deduction_postings
    posting_list.append(Posting(account, amount))

  def _ValidateBeforePrint(self):
    """Verifies that the parsed postings are valid (match total gross/net/deduct)."""
    sum_earnings = sum([posting.amount for posting in self.earning_postings])
    total_gross = self._GetTotalGross()
    if abs(sum_earnings) != abs(total_gross):
      raise ValueError("Sum of earnings ({0}) does not match total gross ({1}) for {2}".format(
          sum_earnings, total_gross, self.paystub_file))
    sum_deductions = sum([posting.amount for posting in self.deduction_postings])
    total_deductions = self._GetTotalDeductions()
    if abs(sum_deductions) != abs(total_deductions):
      raise ValueError(
          "Sum of deductions ({0}) does not match total deductions ({1}) for {2}".format(
              sum_deductions, total_deductions, self.paystub_file))


parser = argparse.ArgumentParser(description="Converts Google paystub PDF into ledger format")
parser.add_argument("paystub_file", type=str,
                    help="Address to the Google paystub PDF which will be parsed.")
parser.add_argument("--mark_stock_virtual", type=bool, default=True,
    help="Marks all stock transactions as virtual, enabling easy-hiding from ledger reports")
parser.add_argument("--debug", type=bool, default=False, help="Prints extra debug info")
parser.add_argument("--skip_validation", type=bool, default=False,
    help="Parses the paystub without performing validation")


if __name__ == "__main__":
  args = parser.parse_args()
  income_parser = IncomeParser(args.paystub_file, args.mark_stock_virtual, args.skip_validation)

  if income_parser.IsStockStub():
    income_parser.AddEarningAccount("Google Stock")
    income_parser.AddDeductionAccount("CPP Gov Pens EE")
    income_parser.AddDeductionAccount("EI Premium EE")
    income_parser.AddDeductionAccount("Federal Tax")
    income_parser.AddDeductionAccount("ON Prov Tax")
    income_parser.AddDeductionAccount("GSU Offset")
    # Quick hack for when an earning account name overflows into a deduction account name.
    income_parser.AddDeductionAccount("Un GSU Offset", ledger_account=DEDUCTIONS_PREFIX+"GSU Offset")
    income_parser.AddDeductionAccount("Reimb GSU Offset", ledger_account=DEDUCTIONS_PREFIX+"GSU Offset")
  else:
    # Whitelist earnings/deductions, anything missed will be caught in validation. This ensures
    # that all postings extracted from paystub are understood. These entries on the paystub will
    # be prepended with the default ledger account prefix in ledger.
    normal_earnings = ["Annual Bonus", "Benefit in Kind", "Canada Vision", "Dental",
        "Education Reimb", "Gifts and Award", "Leave With Pay", "Life Insurance", "Meal Benefit",
        "Peer Bonus", "Regular Pay", "RRSP ER Match", "Spot Bonus A", "Transportation",
        "Vac Entitle Pay", "Vacation Pay"]
    normal_deductions = ["AD&D", "Canada Vision", "CPP Gov Pens EE", "Dental", "EI Premium EE",
        "Federal Income Tax", "Gifts and Award", "Internet Reim", "Life Insurance",
        "Long Term Disab", "Meal Benefit", "Medical"]
    for earning in normal_earnings:
      income_parser.AddEarningAccount(earning)
    for deduction in normal_deductions:
      income_parser.AddDeductionAccount(deduction)

    income_parser.AddEarningAccount("AD&D", ledger_account="Income:Nontaxable:AD&D")
    income_parser.AddEarningAccount("Medical", ledger_account="Income:Nontaxable:Medical")
    income_parser.AddEarningAccount("Survivor Income", ledger_account="Income:Deductions:Survivor Income")
    income_parser.AddDeductionAccount("GSU Refund", ledger_account="Reimbursements:GSU Refund")
    income_parser.AddDeductionAccount("OneTime Charity", ledger_account=CHARITY_ACCOUNT)
    income_parser.AddDeductionAccount("RRSP Contrib", ledger_account=RRSP_ACCOUNT)
    income_parser.AddDeductionAccount("RRSP ER Match", ledger_account=RRSP_ACCOUNT)
    income_parser.AddDeductionAccount("Bonus RRSP Req", ledger_account=RRSP_ACCOUNT)
    income_parser.AddDeductionAccount(
        "Survirvor Incom", ledger_account="Income:Deductions:Survivor Income")
    income_parser.AddDeductionAccount(
        "Survivor Income", ledger_account="Income:Deductions:Survivor Income")
  if args.debug:
    income_parser.PrintDebug()
  income_parser.ParsePostings()
  income_parser.Print()

