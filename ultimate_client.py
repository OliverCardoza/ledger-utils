# A Python client to fetch data from Ultimate MyPay paystubs.
#
# Example usage:
#    from datetime import date
#
#    client = UltimateClient(auth_token="123", domain="example.ultimate.com")
#    pay_data = client.GetDataForPeriod(date(2018, 1, 1), date(2018, 12, 31))
#    print(pay_data)
#    >>> [{
#        'pay_date': date(2018, 1, 15),
#        'pay_period_start': date(2018, 1, 1),
#        'pay_period_end': date(2018, 1, 30),
#        'pay_id': 'ASD123',
#        'net_pay': 1000.00,
#        'earnings': [{
#          'name': 'Salary',
#          'amount': 2000.00,
#        }],
#        'deductions': [{
#          'name': 'Taxes',
#          'amount': 1000.00,
#        }],
#    }]
#
# In order for this to work the mobile website must be enabled for your company. This is necessary
# because the mobile website enables the API used by this client.
# Based on https://github.com/UltimateSoftware/mypay-export

from datetime import date, datetime
from decimal import Decimal
from requests import post

DECIMAL_PRECISION = 2


class UltimateClient(object):
  def __init__(self, auth_token=None, domain=None):
    if auth_token is None:
      raise ValueError("Invalid authToken")
    if domain is None:
      raise ValueError("Invalid domain")
    self.auth_token = auth_token
    self.domain = domain

  def GetPayDataForPeriod(self, start_date, end_date):
    """Returns a list of pay statements within period [start, end] inclusive."""
    if start_date > end_date:
      raise ValueError("Invalid date period: start={0}, end={1}".format(start_date, end_date))

    num_statements = 1
    oldest_date = None
    newest_date = None
    found_enough_data = False
    pay_data = None
    while not found_enough_data:
      if num_statements > 100:
        break
      response_data = self.GetRawPayData_(num_statements)
      pay_data = self.FormatResponseData_(response_data)
      oldest_date = min([stub["pay_date"] for stub in pay_data])
      newest_date = max([stub["pay_date"] for stub in pay_data])
      if oldest_date <= start_date and newest_date >= end_date:
        found_enough_data = True
      num_statements *= 10
    if not found_enough_data:
      raise ValueError(
          "Fetched {0} pay statements but unable to find data for range [{1}, {2}]".format(
              len(pay_data), start_date, end_date))
    filtered_data = [
        stub for stub in pay_data
        if stub["pay_date"] <= end_date and stub["pay_date"] >= start_date]
    filtered_data.sort(key=lambda stub: stub["pay_date"])
    return filtered_data

  def GetRawPayData_(self, num_statements):
    """Makes a request to the Ultimate API and returns the JSON response data."""
    req_url = "https://{0}/mobile/app/services/MobileService.svc/GetYourPayHistory".format(
        self.domain)
    req_data = {"page": 1, "limit": num_statements}
    req_cookies = {"loginToken": self.auth_token}
    res = post(req_url, json=req_data, cookies=req_cookies)
    if res.status_code is not 200:
      raise ValueError(
          "Request to Ultimate API failed: status={0}, body=\n    {1}".format(
              res.status_code, res.text))
    return res.json()

  def FormatResponseData_(self, raw_json):
    """Formats the raw JSON returned by the Ultimate API into a more uniform format."""
    pay_stubs = []
    for raw_pay_stub in raw_json:
      pay_stub = {}
      pay_stub["pay_id"] = raw_pay_stub["PayIdentifier"]
      pay_stub["pay_period_start"] = self.ParseDate_(raw_pay_stub["PeriodStartDate"])
      pay_stub["pay_period_end"] = self.ParseDate_(raw_pay_stub["PeriodEndDate"])
      pay_stub["pay_date"] = self.ParseDate_(raw_pay_stub["PayDate"])
      pay_stub["net_pay"] = round(Decimal(raw_pay_stub["NetPayCurrent"]), DECIMAL_PRECISION)
      pay_stub["earnings"] = self.GetEarnings_(raw_pay_stub)
      pay_stub["deductions"] = self.GetDeductions_(raw_pay_stub)
      pay_stubs.append(pay_stub)
    return pay_stubs

  def GetEarnings_(self, raw_pay_stub):
    """Returns a list of all earning pay items.
    
    NOTE: This ignores "Accruals" which I don't think are included on any Google paystub.
    """
    earnings = []
    self.AddPayItems_(earnings, raw_pay_stub, "Earnings", "PayDescription", "Amount")
    return earnings

  def GetDeductions_(self, raw_pay_stub):
    """Returns a list of all deduction pay items."""
    deductions = []
    self.AddPayItems_(deductions, raw_pay_stub, "Deductions", "DeductionDescription", "EmployeeAmount")
    self.AddPayItems_(deductions, raw_pay_stub, "DeductionTaxes", "Description", "EmployeeAmount")
    # KNOWN ISSUE: Ultimate "Taxes" return taxes that are already included in "DeductionTaxes"
    #              for that reason they are not included in the output data.
    #self.AddPayItems_(deductions, raw_pay_stub, "Taxes", "TaxDescription", "Amount")

    dupe_index = self.GetDuplicateGsuOffsetItemIndex_(deductions)
    if dupe_index is not None:
      del deductions[dupe_index]
    return deductions

  def GetDuplicateGsuOffsetItemIndex_(self, deductions):
    """Returns the index of the duplicate GSU offset item if one exists in the list.

    KNOWN ISSUE: Some pay stubs from 2018 have duplicate "GSU Offset" pay items. This step here
                 removes these duplicates to allow the data to balance to zero.
    """
    gsu_offsets = 0
    for index, item in enumerate(deductions):
      if item["name"] == "GSU Offset":
        gsu_offsets += 1
        if gsu_offsets == 2:
          return index
    return None

  def AddPayItems_(
      self, pay_items, raw_pay_stub, json_list_prop, item_name_prop, item_amount_prop):
    """Adds items from the identified list if the amount is non-zero."""
    # Iterate over items in alphabetical order.
    sorted_items = sorted(raw_pay_stub[json_list_prop], key=lambda item: item[item_name_prop])
    for item in sorted_items:
      amount = Decimal(item[item_amount_prop])
      amount = round(amount, DECIMAL_PRECISION)
      if amount != 0:
        pay_items.append({
          "name": item[item_name_prop],
          "amount": amount,
        })

  def ParseDate_(self, raw_date):
    """Parses a string date (e.g. "1/30/2018") from JSON response into a Python date."""
    return datetime.strptime(raw_date, "%m/%d/%Y").date()


