# A helper class for capital gains management.
#
# The underlying philosophy and implementation for this tool is outlined in
# https://olivercardoza.com/2020/12/04/ledger-and-capital-gains.html
#
# For now a lot of this is quite rough with TODOs to fill in during a
# sale. I may improve this in the future with some automation.


# Account prefix used for all adjusted cost basis accounts.
ACB_ACCOUNT_PREFIX = "Virtual:ACB"
# Account used to track capital gains.
CAPITAL_GAINS_ACCOUNT = "(Virtual:Capital Gains)"


def _GetAcbAccount(stock_symbol, wrap=True):
  """Get the ACB account name optionally wrapped with () for the posting."""
  account = "{0}:{1}".format(ACB_ACCOUNT_PREFIX, stock_symbol)
  return "({0})".format(account) if wrap else account


def CreateBuyString(stock_symbol, total_cost_cad):
  """Returns posting lines to track capital gains for a BUY trade."""
  account = _GetAcbAccount(stock_symbol)
  return "    {account: <48} CAD {total_cost_cad:.4f}\n".format(
      account=account, stock_symbol=stock_symbol,
      total_cost_cad=total_cost_cad)


def CreateSellString(
    stock_symbol, num_units, unit_fmv_cad, sell_fees_cad, total_acb_cad):
  """Returns complete posting lines to track capital gains for a SELL trade.

  The ACB/FMV/fees are all present and included in ledger comments to make this
  more readable and debuggable in the future.
  """
  acb_account = _GetAcbAccount(stock_symbol)
  fmv = num_units * unit_fmv_cad
  capital_gains = fmv - total_acb_cad - sell_fees_cad
  template_string = """    {acb_account: <48} CAD -{total_acb_cad:.4f}
    {capital_gains_account: <48} CAD {capital_gains:.4f}
    ;    capital_gains = fmv - acb - fees
    ;    fmv = {fmv}
    ;    acb = {total_acb_cad}
    ;    fees = {sell_fees_cad}
"""
  return template_string.format(
      acb_account=acb_account, total_acb_cad=total_acb_cad, num_units=num_units,
      capital_gains=capital_gains, fmv=fmv, sell_fees_cad=sell_fees_cad,
      capital_gains_account=CAPITAL_GAINS_ACCOUNT)


def CreateSellTodoString(
    stock_symbol, num_units, unit_fmv_cad, sell_fees_cad):
  """Returns posting lines with TODOs to track capital gains for a SELL trade.
  
  This notably adds two lines, one to deduct cost from the ACB total, and
  another to record the capital gains. These are mostly just TODOs for now.
  Filling this in during processing would break the modular tooling
  assumptions so they are left as manual work with tips for now.
  """
  acb_account = _GetAcbAccount(stock_symbol, wrap=False)
  acb_account_wrapped = _GetAcbAccount(stock_symbol, wrap=True)
  fmv = num_units * unit_fmv_cad
  template_string = """    {acb_account_wrapped: <48} CAD -0.00
    ; TODO: fill in values below.
    ;    acb = total_acb * units_sold/total_units
    ;    units_sold = {num_units}
    ;    total_acb = TODO
    ;        HINT: Find ACB BEFORE this transaction
    ;              `ledger reg {acb_account}`
    ;    total_units = TODO
    ;        HINT: Find total units of ${stock_symbol} held BEFORE this
    ;              transaction in non-registered accounts.
    ;              `ledger reg Assets`
    {capital_gains_account: <48} CAD 0.00
    ; TODO: fill in values below.
    ;    capital_gains = fmv - acb - fees
    ;    fmv = {fmv}
    ;    acb = TODO use value calculated for acb from previous posting
    ;    fees = {sell_fees_cad}
"""
  return template_string.format(acb_account_wrapped=acb_account_wrapped,
      stock_symbol=stock_symbol, num_units=num_units,
      fmv=fmv, sell_fees_cad=sell_fees_cad, acb_account=acb_account,
      capital_gains_account=CAPITAL_GAINS_ACCOUNT)

