package creds

import (
	"bufio"
	"fmt"
	"os"
)

// Creds contain login credentials.
type Creds struct {
	// Login is the security identity, similar to username.
	Login string
	// Password is the security passphrase.
	Password string
}

// MFACode stores data used for multi-factor authentication.
type MFACode struct {
	// Value is the code received.
	Value string
}

// MFACodeFromInput creates an MFACode value from stdin.
func MFACodeFromInput() (*MFACode, error) {
	fmt.Printf("Please enter MFA code: ")
	code, err := bufio.NewReader(os.Stdin).ReadString('\n')
	if err != nil {
		return nil, fmt.Errorf("reading mfa code from stdin: %w", err)
	}
	return &MFACode{Value: code}, nil
}
