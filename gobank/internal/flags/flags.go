package flags

import (
	"time"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks"
)

// DateFlag is a struct for use with pflag.Value to parse
// flag values of the form YYYY-mm-DD.
type DateFlag struct {
	Value time.Time
}

// String returns the string value of the flag.
func (d *DateFlag) String() string {
	return d.Value.String()
}

// Set parses the raw value.
func (d *DateFlag) Set(in string) error {
	t, err := time.Parse("2006-01-02", in)
	if err != nil {
		return err
	}
	d.Value = t
	return nil
}

// Type returns the semantic type for this flag.
func (d *DateFlag) Type() string {
	return "DateFlag"
}

// BankFlag enumerates the types of banks supported.
type BankFlag struct {
	Value banks.Bank
}

// String returns the string value of the flag.
func (b *BankFlag) String() string {
	return string(b.Value)
}

// Set parses the raw value.
func (b *BankFlag) Set(in string) error {
	val, err := banks.FromString(in)
	if err != nil {
		return err
	}
	b.Value = val
	return nil
}

// Type returns the semantic type for this flag.
func (b *BankFlag) Type() string {
	return "BankFlag"
}
