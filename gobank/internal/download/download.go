/* Package download manages the browser's file download functionality. */
package download

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/chromedp/cdproto/browser"
	"github.com/chromedp/chromedp"
)

// Manager coordinates browser file downloads.
// Ensure that each instance is closed upon completion.
type Manager struct {
	initialized bool
	contents    chan []byte
	errChan     chan error
	tmpDir      string
}

func (m *Manager) init() error {
	if m.initialized {
		return nil
	}
	m.contents = make(chan []byte, 1)
	m.errChan = make(chan error, 1)

	tmpDir, err := os.MkdirTemp("", "gobank")
	if err != nil {
		return fmt.Errorf("initializing download manager: %w", err)
	}
	m.tmpDir = tmpDir

	m.initialized = true
	return nil
}

// Close frees up resources used by the download manager.
func (m *Manager) Close() error {
	if !m.initialized {
		return nil
	}
	return os.RemoveAll(m.tmpDir)
}

// Listen begins listening for download events.
// This call does not block until the listen channel is complete.
func (m *Manager) Listen(ctx context.Context) error {
	err := m.init()
	if err != nil {
		return err
	}

	// Configure headless browser downloads. Note that
	// SetDownloadBehaviorBehaviorAllowAndName is preferred here over
	// SetDownloadBehaviorBehaviorAllow so that the file will be named as
	// the GUID.
	downloadParams := &browser.SetDownloadBehaviorParams{
		Behavior:      browser.SetDownloadBehaviorBehaviorAllowAndName,
		DownloadPath:  m.tmpDir,
		EventsEnabled: true,
	}
	err = chromedp.Run(ctx, downloadParams)
	if err != nil {
		return err
	}
	chromedp.ListenTarget(ctx, func(v interface{}) {
		if ev, ok := v.(*browser.EventDownloadProgress); ok {
			if ev.State == browser.DownloadProgressStateCompleted {
				filename := filepath.Join(m.tmpDir, ev.GUID)
				data, err := os.ReadFile(filename)
				if err != nil {
					m.errChan <- fmt.Errorf("reading downloaded file %v: %v", filename, err)
					return
				}
				m.contents <- data
			}
		}
	})
	return nil
}

// WaitForDownload will block until a file download occurs and returns the
// file contents.
func (m *Manager) WaitForDownload(ctx context.Context) ([]byte, error) {
	err := m.init()
	if err != nil {
		return nil, err
	}

	select {
	case contents := <-m.contents:
		return contents, nil
	case err := <-m.errChan:
		return nil, err
	case <-ctx.Done():
		return nil, nil
	}
}
