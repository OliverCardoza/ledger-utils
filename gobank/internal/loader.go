package internal

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/auto"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/creds"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/download"
	"github.com/chromedp/chromedp"
	"github.com/spf13/viper"
)

var (
	execOptions = []chromedp.ExecAllocatorOption{
		chromedp.NoFirstRun,
		chromedp.NoDefaultBrowserCheck,
		//chromedp.Headless,

		chromedp.Flag("disable-background-networking", true),
		chromedp.Flag("enable-features", "NetworkService,NetworkServiceInProcess"),
		chromedp.Flag("disable-background-timer-throttling", true),
		chromedp.Flag("disable-backgrounding-occluded-windows", true),
		chromedp.Flag("disable-breakpad", true),
		chromedp.Flag("disable-client-side-phishing-detection", true),
		chromedp.Flag("disable-default-apps", true),
		chromedp.Flag("disable-dev-shm-usage", true),
		chromedp.Flag("disable-extensions", true),
		chromedp.Flag("disable-features", "site-per-process,Translate,BlinkGenPropertyTrees"),
		chromedp.Flag("disable-hang-monitor", true),
		chromedp.Flag("disable-ipc-flooding-protection", true),
		chromedp.Flag("disable-popup-blocking", true),
		chromedp.Flag("disable-prompt-on-repost", true),
		chromedp.Flag("disable-renderer-backgrounding", true),
		chromedp.Flag("disable-sync", true),
		chromedp.Flag("force-color-profile", "srgb"),
		chromedp.Flag("metrics-recording-only", true),
		chromedp.Flag("safebrowsing-disable-auto-update", true),
		chromedp.Flag("enable-automation", true),
		chromedp.Flag("password-store", "basic"),
		chromedp.Flag("use-mock-keychain", true),

		// Custom
		chromedp.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36"),
		chromedp.Flag("ash-no-nudges", true),
		chromedp.Flag("disable-blink-features", "AutomationControlled"),
	}
)

// Loader configures the browser automation for the specified bank and runs
// through data collection.
type Loader struct {
	done chan string
}

// LoadOptions modifies how the loader downloads data.
type LoadOptions struct {
	// Bank or financial establishment to download from.
	Bank banks.Bank
	// Start of data collection time window.
	Start time.Time
	// Finish of data collection time window.
	Finish time.Time
	// OutFile specifies the filepath for where download data
	OutFile string
}

func (l *Loader) Load(ctx context.Context, opts *LoadOptions) error {
	if l.done == nil {
		l.done = make(chan string, 1)
	}
	log.Printf("Beginning download...")

	auto, err := banks.AutomationForBank(opts.Bank)
	if err != nil {
		return err
	}
	return l.run(ctx, auto, opts)
}

func (l *Loader) run(ctx context.Context, a auto.Automation, opts *LoadOptions) error {
	// Setup
	ctx, cancel := chromedp.NewExecAllocator(ctx, execOptions...)
	defer cancel()
	ctx, cancel = chromedp.NewContext(ctx, chromedp.WithLogf(log.Printf))
	defer cancel()

	// Login + MFA
	err := a.Login(ctx, credsForBank(opts.Bank))
	if err != nil {
		return l.errScreenshot(ctx, err)
	}
	needMFA, err := a.PromptMFA(ctx)
	if err != nil {
		return l.errScreenshot(ctx, err)
	}
	if needMFA {
		code, err := creds.MFACodeFromInput()
		if err != nil {
			return err
		}
		err = a.SubmitMFA(ctx, code)
		if err != nil {
			return l.errScreenshot(ctx, err)
		}
	}

	// Download
	dlManager := &download.Manager{}
	defer dlManager.Close()
	contents, err := a.Download(ctx, &auto.DownloadOptions{
		Start:           opts.Start,
		Finish:          opts.Finish,
		DownloadManager: dlManager,
	})
	if err != nil {
		return l.errScreenshot(ctx, err)
	}
	err = os.WriteFile(opts.OutFile, contents, 0644)
	if err != nil {
		return fmt.Errorf("writing %v: %w", opts.OutFile, err)
	}
	return nil
}

func (l *Loader) errScreenshot(ctx context.Context, err error) error {
	var buf []byte
	screenErr := chromedp.Run(ctx, chromedp.FullScreenshot(&buf, 100))
	if screenErr != nil {
		log.Printf("Failed to capture error screenshot: %v", screenErr)
		return err
	}
	errFile := "error_screenshot.png"
	writeErr := os.WriteFile(errFile, buf, 0644)
	if writeErr != nil {
		log.Printf("Failed to write error screenshot to %v: %v", errFile, writeErr)
		return err
	}
	return fmt.Errorf("%w: capture screenshot %v", err, errFile)
}

// credsForBank pulls the login credentials from .gobank config file or env
// variables.
func credsForBank(b banks.Bank) *creds.Creds {
	prefix := strings.ToUpper(string(b))
	return &creds.Creds{
		Login:    viper.GetString(prefix + "_LOGIN"),
		Password: viper.GetString(prefix + "_PASSWORD"),
	}
}
