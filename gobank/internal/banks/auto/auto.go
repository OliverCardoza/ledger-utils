package auto

import (
	"context"
	"time"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/creds"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/download"
)

// DownloadOptions modify how the download operation is performed.
type DownloadOptions struct {
	// Start of data collection time window.
	Start time.Time
	// Finish of data collection time window.
	Finish time.Time
	// DownloadManager provides a simple interface to manage the browser download
	// functionality. This is only useful if the automation results in a file
	//download.
	DownloadManager *download.Manager
}

// Automation represents the interface each bank integration must implement.
type Automation interface {
	Login(context.Context, *creds.Creds) error
	PromptMFA(context.Context) (bool, error)
	SubmitMFA(context.Context, *creds.MFACode) error
	Download(context.Context, *DownloadOptions) ([]byte, error)
}
