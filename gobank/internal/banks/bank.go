package banks

import (
	"fmt"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/auto"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/eq"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/manulife"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/td"
)

// Bank is an enumeration of supported banks.
type Bank string

const (
	// Unknown value.
	Unknown Bank = "unknown"
	// TD bank: https://td.com
	TD Bank = "td"
	// EQ bank: https://eqbank.ca
	EQ Bank = "eq"
	// Manulife bank: https://manulife.com
	Manulife Bank = "manulife"
)

// FromString parses a value into a Bank instance.
func FromString(in string) (Bank, error) {
	switch in {
	case string(TD):
		return TD, nil
	case string(EQ):
		return EQ, nil
	case string(Manulife):
		return Manulife, nil
	default:
		return Unknown, fmt.Errorf("unsupported value: %v", in)
	}
}

// AutomationForBank returns the automation provider for the given bank.
func AutomationForBank(b Bank) (auto.Automation, error) {
	switch b {
	case TD:
		return &td.Auto{}, nil
	case EQ:
		return &eq.Auto{}, nil
	case Manulife:
		return &manulife.Auto{}, nil
	default:
		return nil, fmt.Errorf("unsupported bank for loader: %v", b)
	}
}
