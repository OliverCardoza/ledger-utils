package eq

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/auto"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/creds"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
)

// Auto holds the primary automation functions to automate
// EQ browser interactions.
type Auto struct{}

// Login goes through the login form.
func (a *Auto) Login(ctx context.Context, creds *creds.Creds) error {
	err := chromedp.Run(ctx,
		chromedp.Navigate(`https://secure.eqbank.ca/login`),
		chromedp.WaitVisible(`//input[@id="emailInput"]`),
		chromedp.SendKeys(`//input[@id="emailInput"]`, creds.Login),
		chromedp.SendKeys(`//input[@id="passwordInput"]`, creds.Password),
		chromedp.Click(`//button[@id="loginButton"]`))
	if err != nil {
		return fmt.Errorf("logging in: %w", err)
	}
	return nil
}

// PromptMFA returns true if MFA submission is necessary.
// This will request a code be sent when true is returned.
// Otherwise if false is returned then MFA is not required.
func (a *Auto) PromptMFA(ctx context.Context) (bool, error) {
	mfaForm := `//input[@id="codeEntry"]`
	accountList := `//div[contains(@class, "accounts")]`
	err := chromedp.Run(ctx, chromedp.WaitVisible(fmt.Sprintf("%s | %s", mfaForm, accountList)))
	if err != nil {
		return false, err
	}
	var nodes []*cdp.Node
	err = chromedp.Run(ctx,
		chromedp.Nodes(mfaForm, &nodes, chromedp.AtLeast(0)))
	if err != nil {
		return false, err
	}
	return len(nodes) != 0, nil
}

// SubmitMFA goes through multi-factor authentication form.
func (a *Auto) SubmitMFA(ctx context.Context, mfa *creds.MFACode) error {
	return chromedp.Run(ctx,
		chromedp.WaitVisible(`//input[@id="codeEntry"]`),
		chromedp.SendKeys(`//input[@id="codeEntry"]`, mfa.Value),
		chromedp.Click(`//button[contains(text(), "Verify")]`))
}

// Download initiates the account activity file download.
func (a *Auto) Download(ctx context.Context, opts *auto.DownloadOptions) ([]byte, error) {
	startStr := opts.Start.Format("02012006")
	finishStr := opts.Finish.Format("02012006")

	err := opts.DownloadManager.Listen(ctx)
	if err != nil {
		return nil, err
	}

	// If savings accounts accordion is collapsed then expand it.
	accordionSelector := `//button[@id="HISA-accordion-btn"]`
	expanded := ""
	ok := true
	err = chromedp.Run(ctx,
		chromedp.WaitVisible(accordionSelector),
		chromedp.AttributeValue(accordionSelector, "aria-expanded", &expanded, &ok))
	if err != nil || !ok {
		return nil, fmt.Errorf("checking accound accordion expansion: %w", err)
	}
	if expanded == "false" {
		// Click doesn't seem to work consistently, but JS does.
		err = chromedp.Run(ctx, chromedp.Evaluate("document.getElementById('HISA-accordion-btn').click()", nil))
		if err != nil {
			return nil, fmt.Errorf("clicking account accordion: %w", err)
		}
	}

	err = chromedp.Run(ctx,
		// Again click, doesn't always seem to work here so use JS.
		chromedp.WaitVisible(`//li[contains(@class, "account")]`),
		chromedp.Evaluate("document.getElementsByClassName('account')[0].click()", nil),
		chromedp.WaitVisible(`//h1[contains(@class, "account-details__title")]`),
		// Wait for the form to completely load. It is visible before it is usable.
		chromedp.Sleep(time.Second*3),
		chromedp.Click(`//button[contains(@class, "range-filter") and contains(., "Last 15 days")]`),
		chromedp.SendKeys(`//input[@id="start-date-input"]`, startStr),
		chromedp.SendKeys(`//input[@id="end-date-input"]`, finishStr),
		chromedp.Click(`//button[contains(@class, "transactions__apply-filters-btn")]`),
		chromedp.Sleep(time.Second*3),
		chromedp.Click(`//i[contains(@class, "icon-download")]`))
	if err != nil {
		return nil, err
	}

	return opts.DownloadManager.WaitForDownload(ctx)
}
