package td

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/auto"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/creds"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
)

// Auto holds the primary automation functions to automate
// TD browser interactions.
type Auto struct{}

// Login goes through the login form.
func (a *Auto) Login(ctx context.Context, creds *creds.Creds) error {
	err := chromedp.Run(ctx,
		chromedp.Navigate(`https://easyweb.td.com/`),
		chromedp.WaitVisible(`//input[@name="username"]`),
		chromedp.SendKeys(`//input[@name="username"]`, creds.Login),
		chromedp.SendKeys(`//input[@formcontrolname="password"]`, creds.Password),
		chromedp.Click(`//button[contains(text(), 'Login')]`, chromedp.NodeVisible))
	if err != nil {
		return fmt.Errorf("logging in: %w", err)
	}
	return nil
}

// PromptMFA returns true if MFA submission is necessary.
// This will request a code be sent when true is returned.
// Otherwise if false is returned then MFA is not required.
func (a *Auto) PromptMFA(ctx context.Context) (bool, error) {
	textMe := `//button[contains(text(), 'Text me')]`
	accountList := `//tduf-balance-summary-account-row-content`
	err := chromedp.Run(ctx, chromedp.WaitVisible(fmt.Sprintf("%s | %s", textMe, accountList)))
	if err != nil {
		return false, err
	}
	var nodes []*cdp.Node
	err = chromedp.Run(ctx,
		chromedp.Nodes(`//button[contains(text(), 'Text me')]`, &nodes, chromedp.AtLeast(0)))
	if err != nil {
		return false, err
	}
	if len(nodes) == 0 {
		return false, nil
	}

	err = chromedp.Run(ctx,
		chromedp.Click(`//button[contains(text(), 'Text me')]`, chromedp.NodeVisible),
		chromedp.WaitVisible(`//input[@formcontrolname="code"]`))
	if err != nil {
		return false, err
	}
	return true, nil
}

// SubmitMFA goes through multi-factor authentication form.
func (a *Auto) SubmitMFA(ctx context.Context, mfa *creds.MFACode) error {
	err := chromedp.Run(ctx,
		chromedp.SendKeys(`//input[@formcontrolname="code"]`, mfa.Value),
		chromedp.Click(`//button[contains(text(), 'Enter')]`, chromedp.NodeVisible))
	if err != nil {
		return fmt.Errorf("submitting mfa: %w", err)
	}
	return nil
}

// Download initiates the account activity file download.
func (a *Auto) Download(ctx context.Context, opts *auto.DownloadOptions) ([]byte, error) {
	err := opts.DownloadManager.Listen(ctx)
	if err != nil {
		return nil, err
	}

	err = chromedp.Run(ctx,
		// click on account row class=uf-balance-summary-account-row-content
		chromedp.Click(`//tduf-balance-summary-account-row-content`, chromedp.NodeVisible),
		chromedp.WaitVisible(`//h1[contains(text(), "Account Activity")]`))
	if err != nil {
		return nil, err
	}
	// check if expanded
	var expanded string
	var found bool
	err = chromedp.Run(ctx, chromedp.AttributeValue(`//a[@id="transSearchLink"]`, "aria-expanded", &expanded, &found))
	if !found {
		return nil, fmt.Errorf("can't find search expansions state")
	}
	if expanded != "true" {
		err = chromedp.Run(ctx, chromedp.Click(`//a[@id="transSearchLink"]`))
		if err != nil {
			return nil, err
		}
	}

	nums := []int{
		opts.Start.Year(),
		int(opts.Start.Month()),
		opts.Start.Day(),
		opts.Finish.Year(),
		int(opts.Finish.Month()),
		opts.Finish.Day(),
	}
	var vals []string
	for _, num := range nums {
		vals = append(vals, strconv.Itoa(num))
	}

	err = chromedp.Run(ctx,
		chromedp.Click(`//input[@id="searchFromRadio"]`, chromedp.NodeVisible),
		chromedp.SetValue(`//select[@name="DateRangeYearFrom"]`, vals[0]),
		chromedp.SetValue(`//select[@name="DateRangeMonthFrom"]`, vals[1]),
		chromedp.SetValue(`//select[@name="DateRangeDayFrom"]`, vals[2]),
		chromedp.SetValue(`//select[@name="DateRangeYearTo"]`, vals[3]),
		chromedp.SetValue(`//select[@name="DateRangeMonthTo"]`, vals[4]),
		chromedp.SetValue(`//select[@name="DateRangeDayTo"]`, vals[5]),
		chromedp.Click(`//div[@id="searchRangeRow"]//input[@value="Search" and @type="submit"]`, chromedp.NodeVisible),
		chromedp.Sleep(time.Second*3))
	if err != nil {
		return nil, err
	}
	err = chromedp.Run(ctx,
		chromedp.SetValue(`//select[@id="ExportTypeSelect"]`, "csv", chromedp.NodeVisible),
		chromedp.Click(`//span[@id="download_button"]`))
	if err != nil {
		return nil, err
	}
	return opts.DownloadManager.WaitForDownload(ctx)
}
