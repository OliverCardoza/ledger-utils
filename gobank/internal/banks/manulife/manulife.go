package manulife

import (
	"context"
	"fmt"
	"log"
	"time"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/banks/auto"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/creds"
	"github.com/chromedp/chromedp"
)

// Auto holds the primary automation functions to automate
// Manulife browser interactions.
type Auto struct{}

// Login goes through the login form.
func (a *Auto) Login(ctx context.Context, creds *creds.Creds) error {
	err := chromedp.Run(ctx,
		chromedp.Navigate(`https://id.manulife.ca/?ui_locales=en-CA&goto=https%3A%2F%2Fportal.manulife.ca%2Fapps%2Fgroupretirement%2Fportal%2Fmember%2Fhandlelogin`),
		chromedp.WaitVisible(`//input[@id="username"]`),
		chromedp.SendKeys(`//input[@id="username"]`, creds.Login),
		chromedp.SendKeys(`//input[@id="password"]`, creds.Password),
		chromedp.Click(`//button[contains(text(), 'Sign in')]`, chromedp.NodeVisible))
	if err != nil {
		return fmt.Errorf("logging in: %w", err)
	}
	return nil
}

// PromptMFA returns true if MFA submission is necessary.
// This will request a code be sent when true is returned.
// Otherwise if false is returned then MFA is not required.
func (a *Auto) PromptMFA(ctx context.Context) (bool, error) {
	return false, nil
}

// SubmitMFA goes through multi-factor authentication form.
func (a *Auto) SubmitMFA(ctx context.Context, mfa *creds.MFACode) error {
	return nil
}

// Download initiates the account activity file download.
func (a *Auto) Download(ctx context.Context, opts *auto.DownloadOptions) ([]byte, error) {
	err := validateDates(opts.Start, opts.Finish)
	if err != nil {
		return nil, err
	}

	var txnHREF string
	var ok bool
	var myAccountURL string
	myAccountHeader := `//a[@id="menutab1" and contains(text(), "My Account")]`
	err = chromedp.Run(ctx,
		chromedp.Click(`//button/span[contains(text(), "Go")]`, chromedp.NodeVisible),
		chromedp.WaitVisible(myAccountHeader),
		chromedp.AttributeValue(`//a[contains(text(), "Transaction Summary")]`, "href", &txnHREF, &ok),
		chromedp.Location(&myAccountURL))
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, fmt.Errorf("unable to get transaction summary href")
	}

	var optionValue string
	optionTextMatch := opts.Start.Format("Jan 02") + ","
	selectXPath := `//form[@name="PeriodForm"]/select[@name="period"]`
	optionXPath := fmt.Sprintf(`%s/option[contains(text(), "%s")]`, selectXPath, optionTextMatch)
	txnPageURL := myAccountURL + txnHREF
	err = chromedp.Run(ctx,
		chromedp.Navigate(txnPageURL),
		chromedp.WaitVisible(selectXPath),
		chromedp.AttributeValue(optionXPath, "value", &optionValue, &ok),
		// Sometimes it seems that SetValue doesn't issue the change event on the select.
		// In this case we can force it because the onchange attribute is used.
		// https://github.com/chromedp/chromedp/issues/607
		chromedp.SetValue(selectXPath, optionValue),
		chromedp.Evaluate(`PeriodForm.submit()`, nil),
		// Sleep to allow the form to update. It's already visible so the normal
		// strategy doesn't work.
		chromedp.Sleep(time.Second))
	if err != nil {
		return nil, err
	}

	log.Printf("TODO: finish from here, currently hangs")
	// Idea: build an <a> and click to download
	// https://stackoverflow.com/questions/15547198/export-html-table-to-csv-using-vanilla-javascript
	// csvData, err := extractCSV(ctx)
	// if err != nil {
	// 	return err
	// }

	chromedp.Run(ctx, chromedp.WaitVisible(`//div[@id="neverexists"]`))
	return nil, fmt.Errorf("stop")
}

func validateDates(start, finish time.Time) error {
	startStr := start.Format("2006-01")
	finishStr := start.Format("2006-01")
	if startStr != finishStr {
		return fmt.Errorf("Manulife only supports snapshots for same month: start=%v, finish=%v", startStr, finishStr)
	}
	if startDay := start.Day(); startDay != 1 {
		return fmt.Errorf("Manulife only supports start day=1 but encountered start_day=%v", startDay)
	}
	finishPlusDay := finish.Add(time.Hour * 24)
	if finishPlusDay.Month() == finish.Month() {
		return fmt.Errorf("Manulife only supports end day as last day of month, finish=%v", finishStr)
	}
	now := time.Now()
	if now.Sub(finish) <= 61*24*time.Hour {
		return fmt.Errorf("Manulife statement for %v is not available (statements available 2 months after date)", finishStr)
	}
	return nil
}
