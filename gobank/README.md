# gobank

Prototyping how to download raw content like csvs directly from bank
websites.

Requires installing Go: <https://go.dev/doc/install>

```bash
go install bitbucket.org/OliverCardoza/ledger-utils/gobank@latest
```

Usage instructions:

```bash
gobank --help
```
