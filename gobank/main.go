package main

import (
	"log"
	"os"

	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal"
	"bitbucket.org/OliverCardoza/ledger-utils/gobank/internal/flags"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	rootCmd = &cobra.Command{
		Use:   "gobank",
		Short: "gobank is a CLI to download bank data",
		CompletionOptions: cobra.CompletionOptions{
			DisableDefaultCmd: true,
		},
	}

	downloadCmd = &cobra.Command{
		Use:   "download",
		Short: "Download bank data into a local file",
		Long: `Download bank data into a local file.

Credentials can be configured using a ~/.gobank file.
The format is dotenv so 1 line per configuration value.
This is the recommended way to pass bank credentials.
For example, the "td" bank option can be configured with a
config file containing two lines such as:

    TD_LOGIN=<access_card_number>
    TD_PASSWORD=<plaintext_password>

If you prefer not to use a config file then you can also set these environment
variables using something like:

    $ read -ps TD_PASSWORD
`,
		Run:     downloadFn,
		Example: "gobank download --bank td --start 2022-01-01 --finish 2022-01-31 --out td.csv",
	}
	startFlag  flags.DateFlag
	finishFlag flags.DateFlag
	bankFlag   flags.BankFlag
)

func init() {
	rootCmd.AddCommand(downloadCmd)
	downloadCmd.Flags().StringP("out", "o", "out.csv", "Output filepath for the downloda")
	downloadCmd.Flags().VarP(&startFlag, "start", "s", "Starting marker for account activity.")
	downloadCmd.Flags().VarP(&finishFlag, "finish", "f", "Finishing marker for account activity.")
	downloadCmd.Flags().VarP(&bankFlag, "bank", "b", "Bank or financial establishment to download from.")
	downloadCmd.MarkFlagRequired("start")
	downloadCmd.MarkFlagRequired("finish")
	downloadCmd.MarkFlagRequired("bank")

	cobra.OnInitialize(initConfig)
}

func initConfig() {
	// Search config in home directory with name ".gobank".
	home, err := os.UserHomeDir()
	cobra.CheckErr(err)
	viper.AddConfigPath(home)
	viper.SetConfigType("dotenv")
	viper.SetConfigName(".gobank")
	// Read environment variables that match.
	viper.AutomaticEnv()

	// If a config file is found, read it in.
	cobra.CheckErr(viper.ReadInConfig())
}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func downloadFn(cmd *cobra.Command, args []string) {
	out, _ := cmd.Flags().GetString("out")
	l := &internal.Loader{}
	err := l.Load(cmd.Context(), &internal.LoadOptions{
		Bank:    bankFlag.Value,
		Start:   startFlag.Value,
		Finish:  finishFlag.Value,
		OutFile: out,
	})
	if err != nil {
		log.Fatalf("failed during download: %v", err)
	}
}
