# Python script to fetch, parse, and ledgerify Ultipro paystubs.
#
# Example usage:
#     python3 income_api.py $YYMM --auth_token=$TOKEN > ./$YYMM_income_ultipro.ledger
#
# In order for this to work the mobile website must be enabled for your company because this
# an API to extract pay data without having to parse PDFs.
# Based on https://github.com/UltimateSoftware/mypay-export

import argparse
import calendar

from datetime import date
from ultimate_client import UltimateClient


GOOGLE_ULTIMATE_DOMAIN = "google-test.ultipro.com"

# Ledger account constants.
NET_PAY_ACCOUNT = "Transfers:Chequing:TD"
RRSP_ACCOUNT = "Transfers:RRSP:Manulife"
EARNING_PREFIX = "Income:Pre-tax:"
DEDUCTION_PREFIX = "Income:Deductions:"
CUSTOM_EARNING_MAPPINGS = {
  "AD&D": "Income:Nontaxable:AD&D",
  "Google Stock Un": "Income:Pre-tax:Google Stock",
  "Medical": "Income:Nontaxable:Medical",
}
CUSTOM_DEDUCTION_MAPPINGS = {
  "Bonus RRSP Req": RRSP_ACCOUNT,
  "GSU Refund": "Reimbursements:GSU Refund",
  "OneTime Charity": "Expenses:Charity",
  "RRSP Contrib": RRSP_ACCOUNT,
  "RRSP ER Match": RRSP_ACCOUNT,
  "Survirvor Incom": "Income:Deductions:Survivor Income",
}


class IncomeLedgerFormatter(object):
  def __init__(self, pay_data, mark_stock_virtual):
    self.pay_data = pay_data
    self.mark_stock_virtual = mark_stock_virtual

  def Validate(self):
    """Validates that the pay data is properly balanced to 0 or else raises an error."""
    for pay_stub in self.pay_data:
      net_pay = pay_stub["net_pay"]
      net_earnings = sum([earning["amount"] for earning in pay_stub["earnings"]])
      net_deductions = sum([earning["amount"] for earning in pay_stub["deductions"]])
      if net_pay != (net_earnings - net_deductions):
        raise ValueError(
            ("Pay stub for date {0} failed validation:\n\t"
             "net_pay: {1}\n\t"
             "net_earnings: {2}\n\t"
             "net_deductions: {3}").format(
                 pay_stub["pay_date"], net_pay, net_earnings, net_deductions))

  def Print(self):
    """Prints the pay data out in Ledger format."""
    for pay_stub in self.pay_data:
      mark_virtual = self.mark_stock_virtual and self.IsStockStub_(pay_stub)
      self.PrintHeader_(pay_stub)
      self.PrintNetPay_(pay_stub)
      self.PrintEarnings_(pay_stub, mark_virtual)
      self.PrintDeductions_(pay_stub, mark_virtual)
      print("") # Footer space

  def PrintHeader_(self, pay_stub):
    header_template = ("{pay_date} * Google paystub"
                       "\n    ; Pay period: {pay_period_start} to {pay_period_end}"
                       "\n    ; Pay Id: {pay_id}")
    print(header_template.format(
        pay_date=pay_stub["pay_date"].isoformat(),
        pay_period_start=pay_stub["pay_period_start"].isoformat(),
        pay_period_end=pay_stub["pay_period_end"].isoformat(),
        pay_id=pay_stub["pay_id"]
    ))

  def PrintNetPay_(self, pay_stub):
    self.PrintPayItem_(NET_PAY_ACCOUNT, pay_stub["net_pay"])

  def PrintEarnings_(self, pay_stub, mark_virtual):
    for earning in pay_stub["earnings"]:
      ledger_account = self.GetLedgerAccount_(earning["name"], mark_virtual, is_earning=True)
      # For double-entry account, earning accounts should be negative amounts.
      amount = -earning["amount"]
      self.PrintPayItem_(ledger_account, amount)

  def PrintDeductions_(self, pay_stub, mark_virtual):
    for deduction in pay_stub["deductions"]:
      ledger_account = self.GetLedgerAccount_(deduction["name"], mark_virtual, is_earning=False)
      self.PrintPayItem_(ledger_account, deduction["amount"])

  def PrintPayItem_(self, ledger_account, amount):
    pay_item_template = "    {0: <68} CAD {1:.2f}"
    print(pay_item_template.format(ledger_account, amount))

  def GetLedgerAccount_(self, pay_item_name, mark_virtual, is_earning):
    custom_mappings = CUSTOM_EARNING_MAPPINGS if is_earning else CUSTOM_DEDUCTION_MAPPINGS
    account = None
    if pay_item_name in custom_mappings:
      account = custom_mappings[pay_item_name]
    else:
      prefix = EARNING_PREFIX if is_earning else DEDUCTION_PREFIX
      account = prefix + pay_item_name
    if mark_virtual:
      account = "[Virtual:{0}]".format(account)
    return account

  def IsStockStub_(self, pay_stub):
    for earning in pay_stub["earnings"]:
      if earning["name"] == "Google Stock Un":
        return True
    return False


parser = argparse.ArgumentParser(description="Fetch and format Ultipro paystubs into ledger format")
parser.add_argument("yymm", type=str,
    help="The last two digits of the year and month you would like to process.")
parser.add_argument("--auth_token", type=str, required=True,
    help="The loginToken cookie copied from an active Ultipro session used for auth.")
parser.add_argument("--skip_validation", type=bool, default=False,
    help="Skips basic verification that all transactions balance to 0 in a paystub.")
parser.add_argument("--mark_stock_virtual", type=bool, default=True,
    help="Marks all stock transactions as virutla, enabling easy-hiding from ledger reports.")


def GetDatePeriod(yymm):
  year = 2000 + int(yymm[:2])
  month = int(yymm[2:])
  start_date = date(year, month, 1)
  end_date = date(year, month, calendar.monthrange(year, month)[1])
  return start_date, end_date


if __name__ == "__main__":
  args = parser.parse_args()

  client = UltimateClient(auth_token=args.auth_token, domain=GOOGLE_ULTIMATE_DOMAIN)
  start_date, end_date = GetDatePeriod(args.yymm)
  pay_data = client.GetPayDataForPeriod(start_date, end_date)

  income_ledger_formatter = IncomeLedgerFormatter(
      pay_data, mark_stock_virtual=args.mark_stock_virtual)
  if not args.skip_validation:
    income_ledger_formatter.Validate()
  income_ledger_formatter.Print()

