# Converts Kraken CSVs into ledger-cli format.
#
# Note that Kraken has 2 CSV file formats that this utility helps with.
# The first is the confusingly named "ledger" format which includes deposits,
# trades, and withdrawals. The second is the "trades" format which has more
# details about trades. The script ignores trades in the "ledger" format and
# instead fills in these details from the "trades" format.
#
# For terminology clarity this class will try to only use "ledger" to mean the
# Kraken output format and will use "ledger-cli" or "LedgerCli" to denote the
# command line interface tool and corresponding format.
#
# Example Usage
#    python3 kraken.py ledgers.csv trades.csv
# 
# Notes: Fiat currency appears to have precision 4 meanwhile crypto has 8.


import argparse
import capital_gains
import collections
import csv
import dataclasses
import datetime
import decimal
import enum
import math


# The number of digits right of decimal point to keep for values.
# May need to increase this based on how granular your XBT values go.
DECIMAL_PRECISION = 4
# Maximum allowed rounding error before throwing an error.
MAXIMUM_ROUNDING_ERROR = 0.1

# The primary Kraken account in LedgerCli.
KRAKEN_ACCOUNT = "Assets:Brokerage:Kraken"
# The account being debitted for deposits into Kraken account.
DEPOSIT_ACCOUNT = "Transfers:Brokerage:Kraken"
# The account being credited for withdrawals out of Kraken account.
WITHDRAWAL_ACCOUNT = "Transfers:Crypto:Ledger"
# The account used for fees and rebates.
FEE_ACCOUNT = "Expenses:Bills and Utilities:Trading Fees"
# The account to attribute small amounts of rounding error.
ROUNDING_ACCOUNT = "Equity:Rounding Errors"


@enum.unique
class TransactionType(enum.IntEnum):
  unknown = 0
  trade_buy = 1
  trade_sell = 2
  deposit = 3
  withdrawal = 4


@dataclasses.dataclass
class LedgerRow:
  """Class representing a single Kraken ledger CSV row.

  Kraken ledger CSV format columns:
  1. txid: always unique, e.g. ABCD1E-FGH2I-J3KLM4 sometimes empty ""
  2. refid: common for both halfs of an exchange, e.g. AB1CDE-F2GHI-JK3L4
  3. time: 2020-11-05 16:24:22
  4. type: (deposit|trade|withdrawal)
  5. subtype:
  6. aclass: currency
  7. asset: (ZCAD|XXBT|XETH)
  8. amount: 1234.56780000
  9. fee: 1.2345
  10. balance: balance specific to asset after txn, e.g. 1234.5678
  """
  txid: str
  refid: str
  time: datetime.datetime
  type: str
  subtype: str
  aclass: str
  asset: str
  amount: decimal.Decimal
  fee: decimal.Decimal
  balance: decimal.Decimal


@dataclasses.dataclass
class TradeRow:
  """Class representing a single Kraken trade CSV row.

  Kraken trade CSV format columns:
  1. txid: always unique, e.g. ABCD1E-FGH2I-J3KLM4
  2. ordertxid: always unique, e.g. ABCD1E-FGH2I-J3KLM4
  3. pair: (XXBTZCAD|XETHZCAD)
  4. time: 2020-11-05 16:24:22
  5. type: (buy|sell)
  6. ordertype: (market|limit)
  7. price: purchase unit price, e.g. 20123.40000
  8. cost: total cost in purchasing currency, e.g. 1234.56000
  9. fee: cost typically in purchasing currency, e.g. 1.23456
  10. vol: purchased units, e.g. 0.05000000
  11. margin: 0.00000
  12. misc: ""
  13. ledgers: "ABC1D2-EF3GH-I4JK5L,ABC1D2-EF3GH-I4JK5L"
  """
  txid: str
  ordertxid: str
  pair: str
  time: datetime.datetime
  type: str
  ordertype: str
  price: decimal.Decimal
  cost: decimal.Decimal
  fee: decimal.Decimal
  vol: decimal.Decimal
  margin: decimal.Decimal
  misc: str
  ledgers: str


@dataclasses.dataclass
class Transaction:
  """Class representing a single ledger-cli transaction.

  This is intended to group potentially multiple entries from the Kraken
  ledger format or a single trade from the Kraken trades format.
  """
  account: str = KRAKEN_ACCOUNT
  fee_account: str = FEE_ACCOUNT
  deposit_account: str = DEPOSIT_ACCOUNT
  withdrawal_account: str = WITHDRAWAL_ACCOUNT
  rounding_account: str = ROUNDING_ACCOUNT
  ledger_rows: list = dataclasses.field(default_factory=list)
  trade_row: TradeRow = None

  @property
  def date(self) -> datetime.datetime:
    if (self.txn_type == TransactionType.deposit or
        self.txn_type == TransactionType.withdrawal):
      # Assumes all rows in the transaction occur on the same date.
      return self.ledger_rows[0].time
    elif (self.txn_type == TransactionType.trade_buy or
          self.txn_type == TransactionType.trade_sell):
      return self.trade_row.time
    else:
      raise ValueError("Unknown transaction type: {0}".format(self.txn_type))

  @property
  def txn_type(self) -> TransactionType:
    if self.ledger_rows:
      first_row_type = self.ledger_rows[0].type
      if first_row_type == "deposit":
        return TransactionType.deposit
      elif first_row_type == "withdrawal":
        return TransactionType.withdrawal
    if self.trade_row:
      if self.trade_row.type == "buy":
        return TransactionType.trade_buy
      elif self.trade_row.type == "sell":
        return TransactionType.trade_sell
    return TransactionType.unknown

  @property
  def payee(self) -> str:
    if self.txn_type == TransactionType.deposit:
      return "Deposit"
    elif self.txn_type == TransactionType.trade_buy:
      return "Trade - Buy"
    elif self.txn_type == TransactionType.trade_sell:
      return "Trade - Sell"
    elif self.txn_type == TransactionType.withdrawal:
      return "Withdrawal"
    else:
      raise ValueError("Unknown transaction type: {0}".format(self.txn_type))

  @property
  def txid(self) -> str:
    if self.trade_row:
      return self.trade_row.txid
    elif self.ledger_rows:
      # Sometimes a ledger row has an empty txid value ""
      # but there is always a non-empty value in the set of rows.
      for ledger_row in self.ledger_rows:
        if ledger_row.txid:
          return ledger_row.txid
    raise ValueError(
        "Unable to find txid within transaction: {0}".format(self))

  @property
  def date_string(self) -> str:
    return self.date.strftime("%Y-%m-%d")

  @property
  def datetime_string(self) -> str:
    return self.date.strftime("%Y-%m-%d %H:%M:%S")

  def GetLedgerCliString(self) -> str:
    if self.txn_type == TransactionType.deposit:
      return self._GetLedgerDepositString()
    elif self.txn_type == TransactionType.withdrawal:
      return self._GetLedgerWithdrawalString()
    elif self.txn_type == TransactionType.trade_sell:
      return self._GetTradeString()
    elif self.txn_type == TransactionType.trade_buy:
      return self._GetTradeString()
    else:
      raise ValueError("Unsupported transaction type: {0}".format(self))

  def _GetLedgerDepositString(self) -> str:
    currency = self._ConvertCurrency(self.ledger_rows[0].asset)
    amount = NewDecimal(self.ledger_rows[0].amount)
    # Hard-coded this hidden fee for wire deposits.
    # Assumes using the Canada domestic wire transfer option:
    # https://support.kraken.com/hc/en-us/articles/360000381846
    fee_amount = NewDecimal(11.50)
    template_string = """{self.date_string} * {self.payee}
    ; Time: {self.datetime_string}
    ; TXID: {self.txid}
    ; REFID: {self.ledger_rows[0].refid}
    {self.deposit_account}
    {self.account: <48} {currency} {amount:.4f}
    {self.fee_account: <48} {currency} {fee_amount:.4f}
"""
    return template_string.format(
        self=self, currency=currency, amount=amount, fee_amount=fee_amount)

  def _GetLedgerWithdrawalString(self) -> str:
    currency = self._ConvertCurrency(self.ledger_rows[0].asset)
    # ABS because we use the amount to show the value being withdrawn and it
    # doesn't make sense to withdraw a negative amount.
    amount = abs(NewDecimal(self.ledger_rows[0].amount))
    template_string = """{self.date_string} * {self.payee}
    ; Time: {self.datetime_string}
    ; TXID: {self.txid}
    ; REFID: {self.ledger_rows[0].refid}
    ; TODO exchange rate for fee
    {self.account}
    {self.withdrawal_account: <48} {currency} {amount:.4f}
    {self.fee_account: <48} {currency} {self.ledger_rows[0].fee:.4f}
"""
    return template_string.format(
        self=self, currency=currency, amount=amount)

  def _GetTradeString(self) -> str:
    acquire_currency = self._ConvertCurrency(self.trade_row.pair[:4])
    spend_currency = self._ConvertCurrency(self.trade_row.pair[4:])
    # Adjust the trade amounts so that they are rounded to match those found in
    # the ledger csv format. The current rounding works for the empirical data
    # I have so far when I compared the rounded values in "ledger" format and
    # raw values from "trades" format.
    adj_fee = DecRoundEven(self.trade_row.fee)
    adj_total = self._GetTotalCost()
    trade_vol = self.trade_row.vol
    # Set trade volume negative for SELL trades because the amount is being
    # removed from your account.
    if self.txn_type == TransactionType.trade_sell:
      trade_vol = -trade_vol
    rounding_error = self._GetTradeRoundingError(
        adj_total, adj_fee, trade_vol, self.trade_row.price)
    capital_gains_postings = self._GetCapitalGainsPostings(
        spend_currency, acquire_currency, trade_vol, adj_total, adj_fee)
    template_string = """{self.date_string} * {self.payee}
    ; Time: {self.datetime_string}
    ; TXID: {self.txid}
    ; ORDERTXID: {self.trade_row.ordertxid}
    {self.account: <48} {spend_currency} {adj_total:.4f}
    {self.account: <48} {acquire_currency} {trade_vol:.4f} @ {spend_currency} {self.trade_row.price:.4f}
    {self.fee_account: <48} {spend_currency} {adj_fee:.4f}
    {self.rounding_account: <48} {spend_currency} {rounding_error}
{capital_gains_postings}"""
    return template_string.format(self=self, acquire_currency=acquire_currency,
        spend_currency=spend_currency, adj_total=adj_total, adj_fee=adj_fee,
        trade_vol=trade_vol, rounding_error=rounding_error,
        capital_gains_postings=capital_gains_postings)

  def _GetCapitalGainsPostings(
      self, spend_currency, acquire_currency, trade_vol, adj_total, adj_fee):
    if spend_currency != "CAD":
      return "    ; TODO ACB and Capital gains\n"
    if trade_vol > 0:
      return capital_gains.CreateBuyString(acquire_currency, abs(adj_total))
    elif trade_vol < 0:
      return capital_gains.CreateSellTodoString(
          acquire_currency, abs(trade_vol), self.trade_row.price, adj_fee)
    else:
      raise ValueError(
          "Unable to create capital gains postings for trade volume: {0}".format(
              trade_vol))

  def _GetTotalCost(self):
    """Gets the total proceeds for a SELL or the total cost for a BUY."""
    cad_row = self._GetCadLedgerRow()
    if self.txn_type == TransactionType.trade_buy:
      # The adj_cost by itself does not include the fee so it is added here.
      # The entire value is also made negative because a BUY involves spending
      # to acquire something.
      return DecRoundDown(cad_row.amount - cad_row.fee)
    elif self.txn_type == TransactionType.trade_sell:
      # The adj_cost by itself needs to have fee removed to get the total
      # proceeds of this SELL.
      return DecRoundDown(cad_row.amount - cad_row.fee)
    raise ValueError(
        "Total cost not defined for transaction type: {0}".format(self.txn_type))

  def _GetCadLedgerRow(self):
    if not self.ledger_rows:
      raise ValueError(
          "Unable to find a Ledger row with ZCAD asset: {0}".format(self.ledger_rows))
    for row in self.ledger_rows:
      if row.asset == "ZCAD":
        return row
    raise ValueError(
        "Unable to find a Ledger row with ZCAD asset: {0}".format(self.ledger_rows))

  def _GetTradeRoundingError(self, adj_total, adj_fee, trade_vol, unit_price):
    remainder = adj_total + adj_fee + NewDecimal(trade_vol * unit_price)
    if remainder > MAXIMUM_ROUNDING_ERROR:
      raise ValueError(
          "Found transaction with large rounding error: {0}".format(
              remainder))
    # Negate so that when remainder is added to balances it cancels out to 0.
    return -remainder


  def _ConvertCurrency(self, currency):
    if currency == "ZCAD":
      return "CAD"
    elif currency == "XXBT":
      return "XBT"
    elif currency == "XETH":
      return "ETH"
    else:
      raise ValueError("Unknown currency: {0}".format(currency))



class KrakenCsvParser(object):
  def __init__(self, ledger_csv_file, trades_csv_file):
    self.ledger_csv_file = ledger_csv_file
    self.trades_csv_file = trades_csv_file
    self.txn_map = {}

  def Parse(self):
    self._ParseLedger()
    self._ParseTrades()

  def _ParseLedger(self):
    with open (self.ledger_csv_file, newline='') as f:
      reader = csv.reader(f)
      for idx, row in enumerate(reader):
        if idx == 0:
          continue
        self._ParseLedgerRow(row)

  def _ParseLedgerRow(self, row):
    time = datetime.datetime.strptime(row[2], "%Y-%m-%d %H:%M:%S")
    ledger_row = LedgerRow(row[0], row[1], time, row[3], row[4], row[5],
                           row[6], NewDecimal(row[7]), NewDecimal(row[8]),
                           NewDecimal(row[9]))
    # For some reason ledger file calls this column "refid" but it maps to
    # the "txid" column in the trade file.
    if ledger_row.refid not in self.txn_map:
      self.txn_map[ledger_row.refid] = Transaction()
    self.txn_map[ledger_row.refid].ledger_rows.append(ledger_row)

  def _ParseTrades(self):
    with open (self.trades_csv_file, newline='') as f:
      reader = csv.reader(f)
      for idx, row in enumerate(reader):
        if idx == 0:
          continue
        self._ParseTradeRow(row)

  def _ParseTradeRow(self, row):
    time = datetime.datetime.strptime(row[3], "%Y-%m-%d %H:%M:%S.%f")
    # Beef up precision to 5 on specific trade values needed for rounding later.
    trade_row = TradeRow(row[0], row[1], row[2], time, row[4], row[5],
                         NewDecimal(row[6]), NewDecimal(row[7], precision=5),
                         NewDecimal(row[8], precision=5), NewDecimal(row[9], precision=5),
                         NewDecimal(row[10]), row[11], row[12])
    if trade_row.txid not in self.txn_map:
      self.txn_map[trade_row.txid] = Transaction()
    self.txn_map[trade_row.txid].trade_row = trade_row

  def Print(self):
    txns = sorted(self.txn_map.values(), key=lambda txn: txn.date)
    for txn in txns:
      ledger_cli_string = txn.GetLedgerCliString()
      if ledger_cli_string:
        print(ledger_cli_string)


def NewDecimal(val, precision=DECIMAL_PRECISION):
  if not val and val != 0:
    return None
  return round(decimal.Decimal(val), precision)


def DecRoundDown(val, precision=DECIMAL_PRECISION):
  shift_factor = pow(10, precision)
  return NewDecimal(math.floor(val * shift_factor) / shift_factor,
      precision=precision)


def DecRoundUp(val, precision=DECIMAL_PRECISION):
  shift_factor = pow(10, precision)
  return NewDecimal(math.ceil(val * shift_factor) / shift_factor,
      precision=precision)


def DecRoundEven(val, precision=DECIMAL_PRECISION):
  """Rounds to nearest digit defaulting to even value to break ties."""
  shift_factor = pow(10, precision)
  return NewDecimal(round(val * shift_factor) / shift_factor,
      precision=precision)


parser = argparse.ArgumentParser(description='Converts Kraken csv files into ledger-cli format')
parser.add_argument('kraken_ledger_file', type=str, 
                    help='Address of the Kraken "ledger" csv file which will be parsed.')
parser.add_argument('kraken_trades_file', type=str,
                    help='Address of the Kraken "trades" csv file which will be parsed.')


if __name__ == '__main__':
  args = parser.parse_args()
  kraken_parser = KrakenCsvParser(args.kraken_ledger_file, args.kraken_trades_file)
  kraken_parser.Parse()
  kraken_parser.Print()


