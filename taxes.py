# Experimental script to perform various tax functions.
#
# This CLI tool is self-describing, to see documentation run:
#     python3 taxes.py -h
# Or to see command-specific help run:
#     python3 taxes.py acb -h
#
# STATUS: Prints out ACB for a given commodity-account pair.
#         python3 taxes.py acb --symbol VAB_TO --account Assets:Brokerage:Questrade


import argparse
import collections
import datetime
import decimal
import price
import subprocess


# A struct to encapsulate a transaction relevant to the ACB calculation.
Transaction = collections.namedtuple(
    'Transaction',
    ['date', 'account', 'commodity','quantity_delta','cost_delta'])


# A map from your Ledger account commodity symbols and those used on exchanges.
# This is used to retrieve the live price using the price.py util.
COMMODITY_MAP = {
  'VAB_TO': 'VAB',
  'VCN_TO': 'VCN',
  'VXC_TO': 'VXC'
}


def _BuildLedgerCommand(account, commodity, start_date=None, end_date=None):
  """Returns a Ledger command which will output a CSV containing data to calculate ACB.

  Formats the output as a CSV with the following columns:
  YYYY-MM-DD,ACCOUNT,COMMODITY,QUANTITY_PURCHASED_OR_SOLD,TOTAL_COST_OR_PROCEEDS
  2020-01-01,Assets:Brokerage,VAB_TO,100,1000
  2020-01-02,Assets:Brokerage,VAB_TO,-50,-500

  Positive values in the latter 2 columns represent purchases which add units and cost to the ACB
  evaluation. Negative values in the latter 2 columns represent sales which remove units and cost
  from the remaining ACB.
  """
  # Finds transaction postings including the desired commodity.
  limit_expression='commodity =~ /{commodity}/'.format(commodity=commodity)
  format_expression=('%(format_date(date, \'%Y-%m-%d\')),'
                     '%(account),'
                     '%(commodity),'
                     '%(quantity(scrub(amount))),'
                     '%(quantity(display_amount))\n')
  start_date_filter = ('' if start_date is None
                       else '--begin %s'.format(start_date.strftime('%Y-%m-%d')))
  end_date_filter = ('' if end_date is None
                     else '--end {0}'.format(end_date.strftime('%Y-%m-%d')))

  return ('ledger register {account} '
          '--limit "{limit}" '
          '--format "{format}" '
          # Display the historical cost or basis value in CAD. This modifies the
          # `display_amount` in the output to specify either:
          # 1. The total value in CAD for a purchase.
          # or
          # 2. The basis value in CAD for a sale (aka. Ledger lot price).
          '--historical -X CAD --basis '
          # Remove <Adjustment> postings from the output. These attempt to reconcile the commodity
          # price specified in the transaction with the price-db file for that date. This means
          # the summation of all transactions may not fully balance out depending on rounding.
          '--no-rounding '
          '{start} {end}').format(
      account=account, limit=limit_expression, format=format_expression, start=start_date_filter,
      end=end_date_filter)


def _ParseTransactionsFromCsv(raw_csv_content):
  lines = raw_csv_content.split('\n')
  transactions = []
  for line in lines:
    columns = line.split(',')
    if len(columns) != 5:
      # Skip empty, or invalid rows.
      continue
    transactions.append(
        Transaction(
            datetime.datetime.strptime(columns[0], '%Y-%m-%d'),
            columns[1], # Account
            columns[2], # Commodity
            int(columns[3]), # Quantity delta
            round(decimal.Decimal(columns[4]), 2))) # Cost delta (rounded to 2 decimal places)
  return transactions


def _GetCurrentPrice(commodity):
  now = datetime.datetime.today()
  now_minus_week = now - datetime.timedelta(days=7)
  symbol = COMMODITY_MAP[commodity] if commodity in COMMODITY_MAP else commodity
  # Retrieve 1 week of data to ensure that least 1 record is returned.
  return round(price.GetPrices(symbol, now_minus_week, now)[-1].amount_in_cad, 2)


def _AcbForCommodity(commodity, account, start_date=None, end_date=None):
  ledger_csv_command = _BuildLedgerCommand(
      account=account, commodity=commodity, start_date=start_date, end_date=end_date)
  # print("Command: \n{0}\n".format(ledger_csv_command))

  p = subprocess.Popen(ledger_csv_command,
                       shell=True,
                       stdout=subprocess.PIPE,
                       stderr=subprocess.STDOUT)
  out, _ = p.communicate()
  # print("Output: \n{0}\n".format(out.decode('utf-8')))

  transactions = _ParseTransactionsFromCsv(out.decode('utf-8'))
  # print("Transactions: \n{0}".format(transactions))

  current_quantity = sum([t.quantity_delta for t in transactions])
  current_total_cost = sum([t.cost_delta for t in transactions])
  fmv = _GetCurrentPrice(commodity)
  acb = 0 if current_total_cost == 0 else round(current_total_cost / current_quantity, 2)
  current_total_value = fmv *current_quantity
  capital_gain = current_total_value - current_total_cost
  print(('{commodity}:\n'
         '    Quantity:      {current_quantity}\n'
         '    FMV:           {fmv}\n'
         '    ACB:           {acb}\n'
         '    Total Cost:    {current_total_cost}\n'
         '    Total Value:   {current_total_value}\n'
         '    Capital Gain:  {capital_gain}\n').format(
      commodity=commodity, current_quantity=current_quantity, fmv=fmv, acb=acb,
      current_total_cost=current_total_cost, current_total_value=current_total_value,
      capital_gain=capital_gain))


def Acb(commodities, account, start_date=None, end_date=None):
  """Calculates the ACB of the commodity in the specified account."""
  print(('Performing ACB calculation with parameters:\n'
         '    Commodities: {0}\n'
         '    Account:     {1}\n'
         '    Start:       {2}\n'
         '    End:         {3}\n').format(commodities, account, start_date, end_date))
  for commodity in commodities:
    _AcbForCommodity(commodity, account, start_date=start_date, end_date=end_date)


def _Date(date_arg):
  try:
    return datetime.datetime.strptime(date_arg, '%Y-%m-%d')
  except ValueError:
    msg = 'Not a valid date: {0}'.format(date_arg)
    raise argparse.ArgumentTypeError(msg)


def _ListString(string_list_arg):
  return string_list_arg.split(',')


parser = argparse.ArgumentParser(description='Performs various tax functions')
subparsers = parser.add_subparsers(title='commands', dest='command', required=True)

acb_parser = subparsers.add_parser('acb',
                                   help=('Calculates the adjusted-cost base (ACB) for a given '
                                         'commodity.'))
acb_parser.add_argument('--commodities', type=_ListString, required=True,
                        help=('The symbol of the stock or currency to filter processing.'
                              'Some examples are VAB_TO, VCN_TO, VXC_TO, and DLR_TO.'))
acb_parser.add_argument('--account', type=str, required=True,
                        help=('Filters transactions to only those including this account. This '
                              'assumes that the same commodity is not held in two non-registered '
                              'accounts.'))
acb_parser.add_argument('-s', '--start', '--from', type=_Date,
                        help='Optional start date to filter processing.')
acb_parser.add_argument('-e', '--end', '--to', type=_Date,
                        help='Optional start date to filter processing.')


if __name__ == '__main__':
  args = parser.parse_args()
  if args.command == 'acb':
    Acb(args.commodities, args.account, start_date=args.start, end_date=args.end)
  else:
    print('Error: unknown command' % args.command)

