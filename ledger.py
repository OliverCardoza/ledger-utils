# Converts CSVs from the Ledger Live, crypto hardware wallet client to ledger-cli.
#
# NOTE: to disambiguate the 2 "ledger" entities the plaintext accounting CLI and format will
# always be denoted by ledger-cli or LedgerCli in this script.
#
# Example usage:
#     python3 ledger.py ledger.csv
#


import argparse
import csv
import dataclasses
import datetime
import decimal


TRANSFER_ACCOUNT = "Transfers:Crypto:Ledger"
LEDGER_ACCOUNT = "Assets:Crypto:Ledger"


@dataclasses.dataclass
class LedgerRow:
  """Class representing a single ledger CSV row.

  CSV columns:
  1. date: 2020-12-01T12:34:56.001Z
  2. raw_currency: (BTC|ETH)
  3. type: IN
  4. amount: 0.1234
  5. fees: 0.000123456
  6. ophash: 1a2b3c4d5e6f7a
  7. account: (Bitcoin 1|Ethereum 1)
  """
  date: datetime.datetime
  raw_currency: str
  type: str
  amount: decimal.Decimal
  fees: decimal.Decimal
  ophash: str
  account: str

  @staticmethod
  def FromRawRow(row):
    if len(row) > 7:
      raise ValueError(
          "Input csv contains >7 columns: remember to DELETE pubx column to preserve privacy!")
    return LedgerRow(
        datetime.datetime.strptime(row[0], "%Y-%m-%dT%H:%M:%S.%fZ"),
        row[1],
        row[2],
        NewDecimal(row[3], precision=8),
        NewDecimal(row[4], precision=8),
        row[5],
        row[6])

  @property
  def date_string(self) -> str:
    return self.date.strftime("%Y-%m-%d")

  @property
  def currency(self) -> str:
    if self.raw_currency == "BTC":
      return "XBT"
    elif self.raw_currency == "ETH":
      return "ETH"
    else:
      raise ValueError("Unknown currency: {0}".format(self.raw_currency))

  def GetLedgerCliString(self) -> str:
    if self.type != "IN":
      raise ValueError("Unknown transaction type: {0}".format(self.type))

    template_str = """{self.date_string} * Crypto Deposit
    ; Time: {self.date}
    ; Hash: {self.ophash}
    ; Fees: {self.currency} {self.fees}
    {transfer_account}
    {ledger_account: <48} {self.currency} {self.amount:.5f}
"""
    return template_str.format(self=self,
                               transfer_account=TRANSFER_ACCOUNT,
                               ledger_account=LEDGER_ACCOUNT)


def NewDecimal(amount_str, precision=8):
  if not amount_str and amount_str != 0:
    return None
  return round(decimal.Decimal(amount_str), precision)


class LedgerCsvParser(object):
  def __init__(self, ledger_file):
    self.ledger_file = ledger_file
    self.rows = []

  def Parse(self):
    self.rows = []
    with open (self.ledger_file, newline='') as f:
      reader = csv.reader(f)
      for idx, row in enumerate(reader):
        if idx == 0:
          # Skip header
          continue
        self.rows.append(LedgerRow.FromRawRow(row))

  def Print(self):
    self.rows = sorted(self.rows, key=lambda row: row.date)
    for row in self.rows:
      print(row.GetLedgerCliString())
 

parser = argparse.ArgumentParser(description="Converts Ledger csv files into ledger-cli format")
parser.add_argument("ledger_file", type=str,
                    help="Address of the Ledger CSV file which will be converted.")


if __name__ == "__main__":
  args = parser.parse_args()
  ledger_parser = LedgerCsvParser(args.ledger_file)
  ledger_parser.Parse()
  ledger_parser.Print()

