A collection of Python scripts I use to convert various financial statments into ledger format.

# Installation

```bash
sudo apt-get install ledger python3 python3-pip pdfgrep python3.11-venv
python3 -m venv ./pyenv
./pyenv/bin/pip3 install requests 'xlrd<2.0.0' yfinance
```

NOTE: 'xlrd<2.0.0' is required because `xlsx` support was dropped starting in 2.0.0

* https://stackoverflow.com/questions/65254535/xlrd-biffh-xlrderror-excel-xlsx-file-not-supported

# Scripts

## income_api.py

**WORK IN PROGRESS**: Processes pay stub information from Ultimate (aka. ultipro). This involves logging into the site
on your corp account and copying the `loginToken` cookie. Example usage:

    TOKEN=<loginToken copied from authenticated session>
    python3 income_api.py 1601 --auth_token=$TOKEN

## kraken.py

Processes Kraken.com CSV transaction data. Kraken offers several CSV formats which contain different types of data. The "Trades" format contains the most information for trades (buy/sells) but it is missing withdrawals and deposits. The "Ledger" format includes these missing transactions but doesn't have sufficient trade data to be usable by itself. For this reason the script requires both trade and ledger files. Example usage:

    python3 kraken.py ledgers.csv trades.csv

## ledger.py

Processes CSV files from the Ledger hardware wallet client (ledger.com). The naming is confusing
but this is referring to a cryptocurrency hardware wallet company. The CSV output includes pubx
data which I prefer not to commit in my accounting repo so I've added checks to ensure this
column is removed from the CSV prior to processing.

    python3 ledger.py ledger.csv

## manulife.js

TODO

## price.py

Utility which can be used as a CLI or imported as a Python library to retrieve price history. It retrieves price data from multiple sources but TBH could probably be consolidated to just use Yahoo Finance.

CLI usage:

    python3 price.py DLR -s 2020-01-01 -e 2020-01-10

Python usage:

    import price
    from datetime import datetime
    prices = price.GetPrices('USD', datetime.today(), datetime.today())

## questrade.py

Processes a Questrade excel file (My accounts > Account activity > Export to Excel) and prints ledger transactions to stdout. Example usage:

    python3 questrade.py 1601_questrade.xlsx

The primary configuration is setting up the desired account names for Questrade accounts and the account you typically transfer funds from. I haven't found a good way to identify what account is providing funds.

## shareworks.py

Processes a Shareworks csv (Activity > Reports > Release Details). Example usage:

    python3 shareworks.py release_report.csv

## stock.py

Processes Morgan Stanley PDF documents for share release, sale, and autosale and prints ledger transactions. Example usage:

    python3 stock.py 20150925_release.pdf
    
Each type of document has its own template and set of regex patterns. Share sale not in the autosale program is difficult and requires some manual calculations of capital gains (hopefully you don't have to do too many of those). All account names are configurable by in the string templates. 

## income.py

**DEPRECATED**: See `income_api.py`

Processes PDF paystubs downloaded from Ultipro and prints ledger transactions. Example usage:

    python3 income.py 2015-06-05_1234.pdf

This script makes a posting for each non-zero amount in the earnings and deductions tables. By default it will name accounts "Income:Pre-tax:<earning>" and "Income:Deductions:<deduction>". There are exceptions for RRSP deductions which are special and have their own variable to set the account name. This script is easily maintainable to add new earning/deduction names as they are introduced. One notable decision was how I decided to handle paystubs regarding stocks see the later section regarding that.


# Dealing with stock data

Stock data comes from three sources: Morgan Stanley, Ultipro, and conversion rates (USD/CAD). I've decided to ignore the Ultipro statements and just use Morgan Stanley transactions in USD. This lets me decide how to handle reporting income in USD (daily/monthly/annual exchange rate). And means I don't have to reconcile a virtual transaction with virtual accounts (GSU offset).
