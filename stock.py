# Script to convert GOOG stock sale/vest PDFs into ledger format.

import argparse
import capital_gains
import dataclasses
import datetime
import decimal
import price
import utils

from pdf_parser import PdfParser


# Account where sale value will be deposited.
DEPOSIT_ACCOUNT = 'Transfers:CanadianForex'
DECIMAL_PRECISION = 4
MAX_ROUNDING_ERROR_CAD = decimal.Decimal(1)


@dataclasses.dataclass
class AutosaleTransaction:
  amount_deposited: decimal.Decimal = None
  award_id: str = None
  capital_gains: decimal.Decimal = None
  deposit_account: decimal.Decimal = None
  fee_cad: decimal.Decimal = None
  fee_usd: decimal.Decimal = None
  fmv_date: str = None
  fmv_usd_to_cad: decimal.Decimal = None
  quantity_released_and_sold: decimal.Decimal = None
  release_date: str = None
  sale_date: str = None
  sale_rounding_error: decimal.Decimal = None
  sale_usd_to_cad: decimal.Decimal = None
  settlement_date: str = None
  tax_cad: decimal.Decimal = None
  tax_usd: decimal.Decimal = None
  # TODO: these variable names are misleading. FMV is actually the sale
  # price. These are actually ACB values which were parsed from the
  # "FMV @ Vest" field in the PDF.
  unit_fmv_usd: decimal.Decimal = None
  unit_fmv_cad: decimal.Decimal = None
  unit_sale_price_usd: decimal.Decimal = None
  unit_sale_price_cad: decimal.Decimal = None
  total_sale_price_cad: decimal.Decimal = None
  value_cad: decimal.Decimal = None

  @property
  def capital_gains_buy_postings(self) -> str:
    return capital_gains.CreateBuyString("GOOG", self.value_cad)

  @property
  def capital_gains_sell_postings(self) -> str:
    return capital_gains.CreateSellString(
        "GOOG", self.quantity_released_and_sold, self.unit_sale_price_cad,
        self.fee_cad, self.value_cad)


def PrintStockRelease(pdf_parser):
  values = {}
  values['award_id'] = pdf_parser.ParseString('Award ID:\s+(\w+)')
  values['release_date'] = pdf_parser.ParseDate(
      'Release Date:\s+(\d+-\w+-\d+)', date_fmt='%d-%b-%Y')
  values['vest_date'] = pdf_parser.ParseDateFirstMatch(
      ['FMV Date:\s+\$[\d.,]+ / (\d+-\w+-\d+)', 'Release Date:\s+(\d+-\w+-\d+)'],
      date_fmt='%d-%b-%Y')
  values['quantity_released'] = pdf_parser.ParseCommodity('Quantity Released:\s+([\d.,]+)')
  # Quantity deposited may be fractional, so up the precision to 4 points to avoid cutoff.
  # https://www.globalequity.org/geo/sites/default/files/5.5-Project-By-the-Slice-Alphabets-Fractional-Share-Project.pdf
  values['quantity_deposited'] = pdf_parser.ParseCommodity('Net Quantity:\s+([\d.,]+)', precision=4)

  vest_date = datetime.datetime.strptime(values['vest_date'], '%Y-%m-%d')
  usd_to_cad = price.GetPrice('USD', vest_date, search_forward=False).amount_in_cad
  values['usd_to_cad'] = usd_to_cad
  values['unit_fmv'] = pdf_parser.ParseCommodityFirstMatch(
      ['FMV @ Vest / FMV Date:\s+\$([\d.,]+)', 'FMV @ Vest:\s+\$([\d.,]+)'])
  values['unit_fmv_cad'] = values['unit_fmv'] * usd_to_cad
  values['tax'] = pdf_parser.ParseCommodity('Total Tax Amount:\s+\$([\d.,]+)') * usd_to_cad
  try:
    values['refund'] = (
        pdf_parser.ParseCommodityFirstMatch(
            ['Excess Amount:\s+\$([\d.,]+)', 'GSU Refund due to Participant:\s+\$([\d.,]+)'])
        * usd_to_cad)
  except ValueError:
    # New stock releases allow fractional shares and hence may not have any cash refund.
    # In this case allow a refund of 0.
    values['refund'] = decimal.Decimal(0)
  values['cad_value'] = values['unit_fmv_cad'] * values['quantity_released']
  values['sale_rounding_error'] = _GetSaleRoundingError([
      round(values['cad_value'] * -1, 4),
      round(values['quantity_deposited'], 4) * round(values['unit_fmv_cad'], 4),
      round(values['tax'], 4),
      round(values['refund'], 4),
  ])

  template = '''{vest_date} * Stock release
    ; Award ID: {award_id}
    ; Release Date: {release_date}
    ; GOOG released = {quantity_released}
    ; 1 GOOG = {unit_fmv} USD
    ; 1 USD = {usd_to_cad} CAD
    Income:Pre-tax:Equity                                  CAD -{cad_value:.4f}
    Assets:Brokerage:Morgan Stanley                        GOOG {quantity_deposited} @ CAD {unit_fmv_cad:.4f}
    Income:Deductions:Federal Income Tax                   CAD {tax:.4f}
    Reimbursements:GSU Refund                              CAD {refund:.4f}
    Equity:Rounding Errors                                 CAD {sale_rounding_error}

'''
  print(template.format(**values))


def PrintStockSale(pdf_parser):
  values = {}
  values['quantity_sold'] = pdf_parser.ParseCommodity('You sold\s+(\d+\.\d+) at a price')
  values['trade_date'] = pdf_parser.ParseDate('on Trade Date (\d+/\d+/\d+)', date_fmt='%m/%d/%y')
  values['settlement_date'] = pdf_parser.ParseDate(
      'Settlement Date\(mm/dd/yy\)\s+(\d+/\d+/\d+)', date_fmt='%m/%d/%y')

  trade_date = datetime.datetime.strptime(values['trade_date'], '%Y-%m-%d')
  usd_to_cad = price.GetPrice('USD', trade_date, search_forward=False).amount_in_cad

  values['usd_to_cad'] = usd_to_cad
  values['amount_deposited'] = pdf_parser.ParseCommodity('Net Amount\s+\$([\d.,]+)')
  values['fee'] = pdf_parser.ParseCommodity('Supp Trans Fee\s+([$.\d]+)') * usd_to_cad
  values['unit_price'] = pdf_parser.ParseCommodity('at a price of\s+(\d+\.\d+) on Trade Date')
  values['unit_price_cad'] = values['unit_price'] * usd_to_cad
  values['deposit_account'] = DEPOSIT_ACCOUNT

  template = '''{trade_date} * Stock sale
    ; Settlement date: {settlement_date}
    ; 1 GOOG = {unit_price} USD
    {deposit_account: <45} USD {amount_deposited} @ CAD {usd_to_cad}
    Assets:Brokerage:Morgan Stanley              GOOG -{quantity_sold} {{CAD TODO}} @ CAD {unit_price_cad:.4f}
    Expenses:Bills and Utilities:Trading Fees    CAD {fee:.4f}
    Income:Pre-tax:Capital Gains                 CAD TODO

'''
  print(template.format(**values))


def PrintStockAutosale(pdf_parser):
  # True if this file uses the legacy format from before around February 2017.
  is_legacy_autosale = pdf_parser.HasMatch('FMV Date')

  autosale_txn = AutosaleTransaction()

  if is_legacy_autosale:
    autosale_txn.fmv_date = pdf_parser.ParseDate(
        'FMV Date:\s+\$[\d.,]+ / (\d+-\w+-\d+)', date_fmt='%d-%b-%Y')
    autosale_txn.unit_fmv_usd = pdf_parser.ParseCommodity('FMV @ Vest / FMV Date:\s+\$([\d.,]+)')
    autosale_txn.fee_usd = pdf_parser.ParseCommodity('Sup Trn Fee:\s+\$([\d.,]+)')
  else:
    autosale_txn.fmv_date = pdf_parser.ParseDate(
        'Release Date:\s+(\d+-\w+-\d+)', date_fmt='%d-%b-%Y')
    autosale_txn.unit_fmv_usd = pdf_parser.ParseCommodity('FMV @ Vest:\s+\$([\d.,]+)')
    # It turns out that this fee is not always deducted. It is included optionally.
    fee_regex = 'SuppTranFee:\s+\$([\d.,]+)'
    if pdf_parser.HasMatch(fee_regex):
      autosale_txn.fee_usd = pdf_parser.ParseCommodity(fee_regex)
    else:
      autosale_txn.fee_usd = decimal.Decimal(0.00)

  autosale_txn.award_id = pdf_parser.ParseString('Award ID:\s+(\w+)')
  autosale_txn.release_date = pdf_parser.ParseDate(
      'Release Date:\s+(\d+-\w+-\d+)', date_fmt='%d-%b-%Y')
  autosale_txn.settlement_date = pdf_parser.ParseDate(
      'Settlement Date:\s+(\d+-\w+-\d+)', date_fmt='%d-%b-%Y')
  autosale_txn.quantity_released_and_sold = pdf_parser.ParseCommodity(
      'Quantity Released / Sold:\s+([\d.,]+)')
  autosale_txn.sale_date = pdf_parser.ParseDate(
      'Sale Date:\s+\$[\d.,]+ /(\d+-\w+-\d+)', date_fmt='%d-%b-%Y')

  autosale_txn.unit_sale_price_usd = pdf_parser.ParseCommodity(
      'WA Sale Price for Quantity Sold ?/Sale Date:\s+\$([\d.,]+)')
  autosale_txn.amount_deposited = pdf_parser.ParseCommodity('Net Proceeds:\s+\$([\d.,]+)')
  autosale_txn.tax_usd = pdf_parser.ParseCommodity('Total Tax Amount:\s+\$([\d.,]+)')

  FillCalculatedAutosaleFields(autosale_txn)
  PrintAutosaleTransaction(autosale_txn)


def PrintManualAutosale():
  autosale_txn = AutosaleTransaction()
  # Presented in the order from a statement.
  autosale_txn.award_id = input("Award ID [C12345]: ")
  autosale_txn.release_date = datetime.datetime.strptime(
      input("Release Date [DD-mmm-YYYY]: "),
      "%d-%b-%Y").strftime("%Y-%m-%d")
  autosale_txn.fmv_date = autosale_txn.release_date
  autosale_txn.settlement_date = datetime.datetime.strptime(
      input("Settlement Date [DD-mmm-YYYY]: "),
      "%d-%b-%Y").strftime("%Y-%m-%d")
  autosale_txn.unit_fmv_usd = utils.parse_commodity(input("FMV @ Vest [$1,234.56]: "))
  autosale_txn.quantity_released_and_sold = utils.parse_commodity(
      input("Quantity Released [1.0000]: "))
  autosale_txn.unit_sale_price_usd = utils.parse_commodity(
      input("WA Sale Price for Quantity Sold [$1,234.56]: "))
  autosale_txn.sale_date = datetime.datetime.strptime(
      input("Sale Date [DD-mmm-YYYY]: "),
      "%d-%b-%Y").strftime("%Y-%m-%d")
  autosale_txn.amount_deposited = utils.parse_commodity(input("Net Proceeds [$1,234.56]: "))
  autosale_txn.tax_usd = utils.parse_commodity(input('Total Tax Amount [$1,234.56]: '))
  autosale_txn.fee_usd = utils.parse_commodity(input('SuppTranFee [$1.23]: '))

  FillCalculatedAutosaleFields(autosale_txn)
  PrintAutosaleTransaction(autosale_txn)


def FillCalculatedAutosaleFields(autosale_txn):
  # Vest currency convert rate
  fmv_date = datetime.datetime.strptime(autosale_txn.fmv_date, '%Y-%m-%d')
  fmv_usd_to_cad = price.GetPrice('USD', fmv_date, search_forward=False).amount_in_cad
  autosale_txn.fmv_usd_to_cad = fmv_usd_to_cad
  # Vest CAD values
  autosale_txn.unit_fmv_cad = autosale_txn.unit_fmv_usd * fmv_usd_to_cad

  # Sale currency convert rate
  sale_date = datetime.datetime.strptime(autosale_txn.sale_date, '%Y-%m-%d')
  sale_usd_to_cad = price.GetPrice('USD', sale_date, search_forward=False).amount_in_cad
  autosale_txn.sale_usd_to_cad = sale_usd_to_cad
  # Sale CAD values
  autosale_txn.unit_sale_price_cad = autosale_txn.unit_sale_price_usd * sale_usd_to_cad
  autosale_txn.tax_cad = autosale_txn.tax_usd * sale_usd_to_cad
  autosale_txn.fee_cad = autosale_txn.fee_usd * sale_usd_to_cad

  # Round the following values to improve transaction consistency. These line items were chosen
  # because they do not represent real amounts transferred (not subject to rounding). Instead they
  # represent artificial values not contained in the source material. For example Canadian value of
  # US commodity for income tax tracking purposes.
  autosale_txn.value_cad = (
      autosale_txn.quantity_released_and_sold * round(autosale_txn.unit_fmv_cad, DECIMAL_PRECISION))
  autosale_txn.total_sale_price_cad = (
      autosale_txn.quantity_released_and_sold * (
          round(autosale_txn.unit_sale_price_cad, DECIMAL_PRECISION)))
  autosale_txn.capital_gains = autosale_txn.value_cad - autosale_txn.total_sale_price_cad
  # The sale uses only 2 real values: the number of units sold and the amount transferred.
  # Everything else is converted. In this scenario I decided that putting everything else as a
  # rounding error still makes sense. The rounding error is rounded to DECIMAL_PRECISON as well
  # for consistency (and because it breaks other things otherwise). This means that rounding errors
  # past the accepted precision are ignored.
  autosale_txn.sale_rounding_error = _GetSaleRoundingError([
      autosale_txn.total_sale_price_cad * -1,
      round(autosale_txn.fee_cad, 4),
      round(autosale_txn.tax_cad, 4),
      autosale_txn.amount_deposited * round(autosale_txn.sale_usd_to_cad, 4)
  ])
  autosale_txn.deposit_account = DEPOSIT_ACCOUNT


def PrintAutosaleTransaction(autosale_txn):
  template = '''{0.fmv_date} * Stock Vest - Autosale
    ; Award ID: {0.award_id}
    ; Release date: {0.release_date}
    ; 1 GOOG = {0.unit_fmv_usd} USD
    ; 1 USD = {0.fmv_usd_to_cad} CAD
    Income:Pre-tax:Equity                        CAD -{0.value_cad:.4f}
    Assets:Brokerage:Morgan Stanley              GOOG {0.quantity_released_and_sold} @ CAD {0.unit_fmv_cad:.4f}
{0.capital_gains_buy_postings}

{0.sale_date} * Stock Sale - Autosale
    ; Award ID: {0.award_id}
    ; Settlement date: {0.settlement_date}
    ; 1 GOOG = {0.unit_sale_price_usd} USD
    ; 1 USD = {0.sale_usd_to_cad} CAD
    Assets:Brokerage:Morgan Stanley              GOOG -{0.quantity_released_and_sold} @ CAD {0.unit_sale_price_cad:.4f}
    Expenses:Bills and Utilities:Trading Fees    CAD {0.fee_cad:.4f}
    Income:Deductions:Federal Income Tax         CAD {0.tax_cad:.4f}
    {0.deposit_account: <44} USD {0.amount_deposited} @ CAD {0.sale_usd_to_cad}
    Equity:Rounding Errors                       CAD {0.sale_rounding_error}
{0.capital_gains_sell_postings}

'''
  print(template.format(autosale_txn))


def _GetSaleRoundingError(lineItemAmounts):
  # Use negative value to balance the real offset.
  sale_rounding_error = -round(sum(lineItemAmounts), 4)
  if sale_rounding_error > MAX_ROUNDING_ERROR_CAD:
    raise ValueError('Rounding error exceeds max allowed: {0:4f}'.format(
        sale_rounding_error))
  return sale_rounding_error


parser = argparse.ArgumentParser(description='Converts Morgan Stanley PDF into ledger format')
parser.add_argument('brokerage_file', type=str,
                    help='Address of the Morgan Stanley PDF which will be parsed.')
parser.add_argument('--manual_autosale', type=bool,
                    help='Enables manual entry of an autosale transaction rather than PDF parsing.')

if __name__ == '__main__':
  args = parser.parse_args()
  if args.manual_autosale:
    PrintManualAutosale()
    quit()

  filepath = args.brokerage_file
  pdf_parser = PdfParser(filepath)

  is_stock_release = utils.pdfgrep('Release Detail Report', filepath)
  is_stock_sale = utils.pdfgrep(
      'You sold\s+\d+\.\d+ at a price of\s+\d+\.\d+', filepath)
  is_autosale = utils.pdfgrep('Release Confirmation', filepath)

  if is_stock_release:
    PrintStockRelease(pdf_parser)
  elif is_stock_sale:
    PrintStockSale(pdf_parser)
  elif is_autosale:
    PrintStockAutosale(pdf_parser)
  else:
    raise ValueError('Unknown stock file format, unable to parse')
