import decimal
import re
import subprocess


def pdfgrep(pattern, filepath):
  p = subprocess.Popen(['pdfgrep', '-o', '-P', pattern, filepath],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.STDOUT)
  out, _ = p.communicate()
  return out.decode('utf-8')


def pdfgrep_match(pattern, filepath):
  out = pdfgrep(pattern, filepath)
  match = re.search(pattern, out)
  if not match or match.lastindex == None:
    return None
  capture_groups = []
  for i in range(1, match.lastindex + 1):
    capture_groups.append(match.group(i))
  return capture_groups


def pdfgrep_parse(regex, filepath):
  match = pdfgrep_match(regex, filepath)
  if not match:
    raise ValueError('No matches in {0} for pattern: {1}'.format(filepath, regex))
  raw_match = match[0]
  return raw_match


def parse_commodity(raw_match, precision=2):
  str_amount = re.sub('[\$,]', '', raw_match)
  if str_amount.startswith('(') and str_amount.endswith(')'):
    str_amount = '-' + str_amount[1:-1]
  dec_amount = decimal.Decimal(str_amount)
  return round(dec_amount, precision)

