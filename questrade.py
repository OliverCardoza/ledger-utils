# Converts Questrade Excel files (xlsx) into ledger format.
# 
# Columns:
#  1. Transaction Date: 13/02/2016 12:00:00 AM
#  2. Settlement Date: 13/02/2016 12:00:00 AM
#  3. Action: (Buy|CON|BRW|DEP|Sell)
#  4. Symbol: VAB.TO
#  5. Description: VANGUARD CDN AGGREGATE BD INDEX ETF TR UNIT WE ACTED AS AGENT
#  6. Quantity: 5.00000
#  7. Price: 25.99000000
#  8. Gross Amount: -126.00
#  9. Commision: -0.33
# 10. Net Amount: -126.33
# 11. Currency: CAD
# 12. Account #: 12345678
# 13. Activity Type: (Trades|Deposits|Dividends|Withdrawals|Other|Interest|FX conversion)
# 14. Account Type: (Individual TFSA|Individual margin|Individual RRSP)

import argparse
import capital_gains
import enum
import datetime
import decimal
import xlrd

# The ledger account which tracks my balance in Questrade margin account.
BROKERAGE_ACCOUNT = "Assets:Brokerage:Questrade"
# The top-level account which tracks my balance in Questrade TFSA account.
TFSA_ACCOUNT = "Assets:TFSA:Questrade"
# The tag used to track TFSA contributions.
TFSA_CONTRIBUTION_TAG = "TFSA_Contributions"
# The top-level account which tracks my balance in Questrade RRSP account.
RRSP_ACCOUNT = "Assets:RRSP:Questrade"
# The tag used to track RRSP contributions.
RRSP_CONTRIBUTION_TAG = "RRSP_Contributions:TODO"
# The account where margin interest should be dumped.
INTEREST_ACCOUNT = "Expenses:Bills and Utilities:Brokerage Interest"
# The account where commission fees should be dumped.
COMMISSION_ACCOUNT = "Expenses:Bills and Utilities:Trading Fees"
# The account used for fees and rebates.
FEE_REBATE_ACCOUNT = "Expenses:Bills and Utilities:Trading Fees"
# Maps the "Account Type" column to a ledger account.
BROKERAGE_MAP = {
  "Individual margin": BROKERAGE_ACCOUNT,
  "Individual RRSP": RRSP_ACCOUNT,
  "Individual TFSA": TFSA_ACCOUNT,
}
# The account being debitted for deposits in Questrade accounts. This is where all deposits
# into Questrade are pulled from to zero the transaction. A common account identifier is used for
# all Questrade accounts because it is often difficult to determine from the sender side where the
# money is going (TFSA or Margin?).
DEPOSIT_ACCOUNT = "Transfers:Questrade"
# The account being debitted for dividends for the given Questrade account.
DIVIDEND_MAP = {
  "Individual margin": "Income:Pre-tax:Brokerage dividends",
  "Individual RRSP": "Income:Nontaxable:RRSP dividends",
  "Individual TFSA": "Income:Nontaxable:TFSA dividends",
}
# The account being creditted for withdrawals in the given Questrade account.
WITHDRAWAL_MAP = {
  "Individual margin": "Transfers:Questrade",
  "Individual RRSP": "Transfers:Questrade",
  "Individual TFSA": "Transfers:Questrade",
}


@enum.unique
class TransactionType(enum.IntEnum):
  trade = 1
  deposit = 2
  dividend = 3
  withdrawal = 4
  interest = 5
  feerebate = 6
  fxconversion = 7


class Transaction(object):
  def __init__(self, row):
    self._ParseRowFields(row)
    self._ActivityTypeProcessing()

  def _ParseRowFields(self, row):
    self.transaction_date = self._NormalizeDate(row[0].value[:10])
    self.settlement_date = self._NormalizeDate(row[1].value[:10])
    self.action = row[2].value
    self.symbol = row[3].value.replace('.', '_')
    self.description = row[4].value
    self.quantity = decimal.Decimal(row[5].value)
    self.price = decimal.Decimal(row[6].value)
    self.gross_amount = decimal.Decimal(row[7].value)
    self.commission = decimal.Decimal(row[8].value)
    self.net_amount = decimal.Decimal(row[9].value)
    self.currency = row[10].value
    self.account_number = row[11].value
    self.activity_type = row[12].value
    self.account_type = row[13].value
    # Fix bad commodity symbol sometime present.
    if self.symbol == "H038778":
      self.symbol = "DLR_TO"

  def _NormalizeDate(self, date_string):
    """Normalizes a date string into YYYY-MM-DD format.

    At some point in November/December 2019 it seems like the format changed from DD/MM/YYYY
    to YYYY-MM-DD (which is of course superior). This function can accept a param in either format
    and ensure they always come out in YYYY-MM-DD.
    """
    try:
      return datetime.datetime.strptime(date_string, "%Y-%m-%d").strftime("%Y-%m-%d")
    except ValueError:
      return datetime.datetime.strptime(date_string, "%d/%m/%Y").strftime("%Y-%m-%d")

  def _ActivityTypeProcessing(self):
    self.ignore = False
    if self.activity_type == "Trades":
      self.type = TransactionType.trade
      self.payee = "Brokerage trade"
    elif self.activity_type == "Deposits":
      self.type = TransactionType.deposit
      self.payee = "Brokerage deposit"
      self.deposit_account = DEPOSIT_ACCOUNT
    elif self.activity_type == "Dividends":
      self.type = TransactionType.dividend
      self.payee = "Dividends"
      if self.account_type in DIVIDEND_MAP:
        self.dividend_account = "{0}:{1}".format(DIVIDEND_MAP[self.account_type], self.symbol)
      else:
        raise ValueError("Account not mentioned in DIVIDEND_MAP: {0}".format(self.account_type))
    elif self.activity_type == "Withdrawals":
      self.type = TransactionType.withdrawal
      self.payee = "Transfer"
      if self.account_type in WITHDRAWAL_MAP:
        self.withdrawal_account = WITHDRAWAL_MAP[self.account_type]
      else:
        raise ValueError("Account not mentioned in WITHDRAWAL_MAP: {0}".format(self.account_type))
    elif self.activity_type == "Interest":
      self.type = TransactionType.interest
      self.payee = "Brokerage interest"
      self.interest_account = INTEREST_ACCOUNT
    elif self.activity_type == "Other" and self.action == "BRW" and self.symbol == "DLR":
      # Ignore the journalling transactions which transfer between cross-listed DLR/DLR.U entries
      self.ignore = True
    elif self.activity_type == "Fees and rebates":
      self.type = TransactionType.feerebate
      self.payee = "Fees and rebates"
      self.fee_rebate_account = FEE_REBATE_ACCOUNT
    elif self.activity_type == "FX conversion":
      self.type = TransactionType.fxconversion
      self.payee = "FX conversion"
    else:
      raise ValueError("Unknown activity_type: {0}".format(self.activity_type))

    if self.account_type in BROKERAGE_MAP:
      self.brokerage_account = BROKERAGE_MAP[self.account_type]
    else:
      raise ValueError("Account not mentioned in BROKERAGE_MAP: {0}".format(self.account_type))

  def PrintLedger(self):
    if self.ignore:
      return

    template_string = """{0.settlement_date} * {0.payee}
    {0.brokerage_account: <48} {0.currency} {0.net_amount}
"""
    if self.brokerage_account == TFSA_ACCOUNT and self.type == TransactionType.deposit:
      # Tag contributions to TFSA. This allows for easy checking of contribution room via
      # $ ledger bal %/$TFSA_TAG/
      # I consider this more preferrable than a virtual account.
      self.tfsa_contribution_tag = TFSA_CONTRIBUTION_TAG
      template_string += "      ; :{0.tfsa_contribution_tag}:\n"
    if self.brokerage_account == RRSP_ACCOUNT and self.type == TransactionType.deposit:
      # Tag contributions to RRSP. This allows for easy checking of contribution room via
      # $ ledger bal %/$RRSP_TAG/
      self.rrsp_contribution_tag = RRSP_CONTRIBUTION_TAG
      template_string += "      ; :{0.rrsp_contribution_tag}:\n"
    if self.commission:
      # Negate commission for 2-entry accounting. For example a commission value
      # of -0.33 needs to be displayed as "Trading Fees   CAD 0.33"
      self.commission *= -1
      self.commission_account = COMMISSION_ACCOUNT
      template_string += "    {0.commission_account: <48} {0.currency} {0.commission}\n"

    if self.type == TransactionType.trade:
      template_string += "    {0.brokerage_account: <48} {0.symbol} {0.quantity:.2f} @ {0.currency} {0.price:.8f}\n"
      if self._IsNonRegisteredTrade():
        template_string += self._GetCapitalGainsPostings()
    elif self.type == TransactionType.deposit:
      template_string += "    {0.deposit_account}\n"
    elif self.type == TransactionType.dividend:
      template_string += "    {0.dividend_account}\n"
    elif self.type == TransactionType.withdrawal:
      template_string += "    {0.withdrawal_account}\n"
    elif self.type == TransactionType.interest:
      self.interest_account = INTEREST_ACCOUNT
      template_string += "    {0.interest_account}\n"
    elif self.type == TransactionType.feerebate:
      template_string += "    {0.fee_rebate_account}\n"
    elif self.type == TransactionType.fxconversion:
      # Leave in a todo in the generate transaction. To do this properly would require merging
      # information across multiple line items which does not fit the current abstraction nicely.
      # Since these transactions are expected to be fairly rare, choosing not to deal with it in
      # automation.
      template_string += "    {0.brokerage_account: <48} TODO! CAD|USD TOTAL_AMOUNT @ CAD|USD UNIT_PRICE\n"
    print(template_string.format(self))

  def _IsNonRegisteredTrade(self):
    return self.type == TransactionType.trade and self.brokerage_account == BROKERAGE_ACCOUNT

  def _GetCapitalGainsPostings(self):
    if self.currency != "CAD":
      # Only handle transactions in CAD for now. Both require conversions to support non-CAD.
      return "    ; TODO ACB and Capital gains\n"
    if self.quantity > 0:
      return capital_gains.CreateBuyString(self.symbol, abs(self.net_amount))
    elif self.quantity < 0:
      return capital_gains.CreateSellTodoString(
          self.symbol, abs(self.quantity), abs(self.price), abs(self.commission))
    else:
      raise ValueError("Trade with 0 units sold/bought? {0}".format(self))


parser = argparse.ArgumentParser(description='Converts Questrade xlsx files into ledger format')
parser.add_argument('questrade_file', type=str, 
                    help='Address of the Questrade excel (xlsx) file which will be parsed.')


if __name__ == '__main__':
  args = parser.parse_args()
  book = xlrd.open_workbook(args.questrade_file)
  sh = book.sheet_by_index(0)
  transactions = []
  
  for rx in range(sh.nrows):
    if rx == 0: continue
    transactions.append(Transaction(sh.row(rx)))
  transactions.sort(key=lambda t: t.settlement_date)
  for t in transactions:
    t.PrintLedger()
