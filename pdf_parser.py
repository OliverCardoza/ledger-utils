# Classes and code used to parse values out of a PDF and then format them
import datetime
import re
import utils


class PdfParser(object):
  def __init__(self, pdf_filepath):
    self.pdf_filepath = pdf_filepath

  def HasMatch(self, regex):
    return utils.pdfgrep(regex, self.pdf_filepath)

  def ParseStringFirstMatch(self, regexes):
    """Returns the first matching regex as a string."""
    errors = []
    for regex in regexes:
      try:
        return self.ParseString(regex)
      except ValueError as error:
        # ignore error, try next regex
        errors.append(error)
    raise ValueError("No matching pattern in list: {0}".format(errors))

  def ParseString(self, regex):
    return utils.pdfgrep_parse(regex, self.pdf_filepath)

  def ParseDateFirstMatch(self, regexes, date_fmt=None):
    """Returns the first matching regex as a string-formatted date."""
    errors = []
    for regex in regexes:
      try:
        return self.ParseDate(regex, date_fmt=date_fmt)
      except ValueError as error:
        # ignore error, try next regex
        errors.append(error)
    raise ValueError("No matching pattern in list: {0}".format(errors))

  def ParseDate(self, regex, date_fmt=None):
    raw_match = utils.pdfgrep_parse(regex, self.pdf_filepath)
    return datetime.datetime.strptime(raw_match, date_fmt).strftime('%Y-%m-%d')

  def ParseCommodityFirstMatch(self, regexes, precision=2):
    """Returns the first matching regex as a Decimal."""
    errors = []
    for regex in regexes:
      try:
        return self.ParseCommodity(regex, precision=precision)
      except ValueError as error:
        # ignore error, try next regex
        errors.append(error)
    raise ValueError("No matching pattern in list: {0}".format(errors))

  def ParseCommodity(self, regex, precision=2):
    raw_match = utils.pdfgrep_parse(regex, self.pdf_filepath)
    return utils.parse_commodity(raw_match, precision=precision)

  def MaybeParseCommodity(self, regex, precision=2):
    raw_matches = utils.pdfgrep_match(regex, self.pdf_filepath)
    if not raw_matches or len(raw_matches) != 1:
      # Expect exactly one match to pull the commodity.
      return None
    return utils.parse_commodity(raw_matches[0], precision=precision)

