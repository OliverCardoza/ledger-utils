/**
 * This script will print Manulife RRSP ledger transactions to the console when
 * run on the Manulife transactions page.
 *
 * Steps:
 *   1. Go to www.manulife.ca/GroupBenefits
 *   2. Click Sign in button then "Group Retirement" -> "Plan Member".
 *   3. Sign in.
 *   4. My Account -> Transaction History -> Transaction Summary
 *   5. Click 'Select a Policy' then select one.
 *   6. Select a period or investment using the drop downs.
 *   7. Copy-paste this script into the browser console.
 *   8. View sweet sweet CSV transactions!
 */

var RRSP_ACCOUNT = 'Assets:RRSP:Manulife';
// Where money comes into RRSP from.
var RRSP_INCOME_ACCOUNT = 'Transfers:RRSP:Manulife';
var RRSP_CAPITAL_GAINS_ACCOUNT = 'Income:Tax Deferred:RRSP Capital Gains';
var RRSP_INTEREST_ACCOUNT = 'Income:Tax Deferred:RRSP Interest';
var RRSP_CONTRIBUTION_TAG = 'RRSP_Contributions';
var FEES_ACCOUNT = 'Expenses:Bills and Utilities:Trading Fees';
var CORRECTIONS_ACCOUNT = 'Equity:Unknown';

var createSpace = function(num) {
  var space = '';
  for (var i = 0; i < num; i++) {
    space += ' ';
  }
  return space;
};

var getText = function(element) {
  if (!element || !element.innerText) {
    return '';
  }
  return element.innerText.replace(',', '').trim();
};

var getAmount = function(amountString) {
  if (!amountString) {
    return null;
  }
  return Number.parseFloat(amountString).toFixed(2);
};

var convertRowToCsv = function(row) {
  var values = row.children;
  var returnString = '';
  for (var i = 0; i < values.length; i++) {
    returnString += getText(values[i]) + ',';
  }
  returnString += '\n';
  return returnString;
};

var convertRowToLedger = function(row) {
  var values = row.children;
  if (values.length < 5) {
    // skip empty line
    return;
  }
  var transaction = {};
  transaction.date = new Date(values[0].innerText).toISOString().slice(0,10);
  var description = getText(values[2]);
  var amountOut = getAmount(getText(values[3]));
  var amountIn = getAmount(getText(values[4]));

  if (amountIn) {
    transaction.amountIn = amountIn;
  }
  if (amountOut) {
    // Use ABS here because this is recorded as a negative number going out of RRSP.
    // In Ledger format it is marked as a positive number paid to payee.
    transaction.amountOut = Math.abs(amountOut);
  }

  if (description === 'Contribution' || description === 'Bonus Contribution') {
    transaction.payee = 'RRSP Contribution';
    transaction.from = RRSP_INCOME_ACCOUNT;
  } else if (description === 'Interest Earned') {
    transaction.payee = 'Interest';
    transaction.from = RRSP_INTEREST_ACCOUNT;
  } else if (description === 'Investment Gain / Loss') {
    transaction.payee = 'Investment Gain / Loss';
    transaction.from = RRSP_CAPITAL_GAINS_ACCOUNT;
  } else if (description === 'Investment fee change') {
    // skip line describing investment change
    return;
  } else if (description === 'Mature Deposit') {
    transaction.payee = 'Mature Deposit';
    transaction.from = RRSP_ACCOUNT;
  } else if (description === 'Fund Transfer') {
    transaction.payee = 'Fund Transfer';
    transaction.from = RRSP_ACCOUNT;
  } else if (description === 'Manulife Service Fee') {
    transaction.payee = 'Manulife Service Fee';
    transaction.from = FEES_ACCOUNT;
  } else if (description === 'Correction Deposit') {
    transaction.payee = 'Correction Deposit';
    transaction.from = CORRECTIONS_ACCOUNT;
  } else {
    throw Error(`Unknown line item, with description: "${description}"`);
  }

  transaction.to = RRSP_ACCOUNT;

  var space = createSpace(60 - transaction.to.length);
  var returnString = `${transaction.date} * ${transaction.payee}\n`;
  returnString += createLineItem(transaction.from, transaction.amountOut);
  returnString += createLineItem(transaction.to, transaction.amountIn);

  if (amountOut && amountIn && Math.abs(amountOut) != Math.abs(amountIn)) {
    // Fees are only expected for fund transfers.
    if (description != 'Fund Transfer') {
      throw Error(
        `Unknown fee for line item: description="${description}",` +
        ` amountOut=${amountOut},` +
        ` amountIn=${amountIn}`);
    }
    var feeAmount = (Math.abs(amountOut) - Math.abs(amountIn)).toFixed(2);
    returnString += createLineItem(FEES_ACCOUNT, feeAmount);
  }

  // Add RRSP contribution tag if applicable. The TODO tag is a reminder to mark the contribution
  // year. This can't be automated because contributions during the first 60 days of the year can
  // count to either the current or previous year.
  if (transaction.payee === 'RRSP Contribution') {
    var tag = RRSP_CONTRIBUTION_TAG;
    returnString += `      ; :${tag}:TODO:\n`
  }
  returnString += '\n';
  return returnString;
};

var createLineItem = function(account, optionalAmount) {
  if (optionalAmount) {
    var space = createSpace(60 - account.length);
    return `    ${account}${space}CAD ${optionalAmount}\n`;
  } else {
    return `    ${account}\n`;
  }
};

var main = function() {
  var tables = document.getElementById('lotusMain')
      .getElementsByTagName('iframe')[0]
      .contentDocument
      .querySelectorAll('table table');
  var hasEstimateWarning = tables.length === 6;
  var rows = tables[hasEstimateWarning ? 4 : 3].querySelectorAll('tr');

  var transactionString = '';
  rows.forEach(function(row) {
    transactionString += '; ' + convertRowToCsv(row);
  });
  transactionString += '\n';

  // Skip first 3 rows (header, blank, opening balance) and last two rows
  // (balance, blank).
  for (var i = 3; i < rows.length-2; i++) {
    var ledgerRow = convertRowToLedger(rows[i]);
    if (ledgerRow) {
      transactionString += ledgerRow;
    }
  }

  // Add a newline because without it ledger sometimes has trouble parsing the last transaction
  // amounts.
  transactionString += '\n';

  console.log(transactionString);
};

main();
